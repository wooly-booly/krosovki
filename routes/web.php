<?php
// \Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
//     echo'<pre>';
//     var_dump($query->sql);
//     // var_dump($query->bindings);
//     // var_dump($query->time);
//     echo'</pre>';
// });


/*** STORE SECTION ***/
Route::get('/store', 'Store\StoreController@index')->name('store');

/* Category */
Route::get('/category/{category}/{category_url_key}', 'Store\CategoriesController@index')->name('store.category');

/* Product */
Route::get('/', 'Store\ProductsController@productList')->name('store.product.list');
Route::get('/product/{product}/quickview', 'Store\ProductsController@quickView')->name('store.product.quickview');
Route::get('/product/{product}/{product_url_key?}', 'Store\ProductsController@singleProduct')->name('store.product');

/* Cart */
Route::get('/cart', 'Store\CartController@cart')->name('store.cart');
Route::get('/cart/info', 'Store\CartController@info')->name('store.cart.info');
Route::post('/cart/add/{product}', 'Store\CartController@add')->name('store.cart.add');
Route::post('/cart/remove/{product}', 'Store\CartController@remove')->name('store.cart.remove');
Route::get('/cart/update/{product}', 'Store\CartController@update')->name('store.cart.update');

/* WishList */
Route::get('/wishlist', 'Store\WishListController@wishlist')->name('store.wishlist');
Route::get('/wishlist/add/{product}', 'Store\WishListController@add')->name('store.wishlist.add');
Route::get('/wishlist/remove/{product}', 'Store\WishListController@remove')->name('store.wishlist.remove');

/* Checkout */
Route::get('/checkout', 'Store\CheckoutController@checkout')->name('store.checkout');
Route::post('/checkout/confirm', 'Store\CheckoutController@confirm')->name('store.checkout.confirm');
Route::get('/checkout/success', 'Store\CheckoutController@success')->name('store.checkout.success');

/* Brand */
Route::get('/brand/{brand}/{brand_url_key?}', 'Store\BrandsController@brand')->name('store.brand');

/* Page */
Route::get('/page/{page_url_key}', 'Store\PagesController@index')->name('store.page');

/* Blog */
Route::get('/article/{article}/{url_key?}', 'Store\ArticlesController@singleArticle')->name('store.article');
Route::get('/articles', 'Store\ArticlesController@articleList')->name('store.article.list');

/*** BACKEND SECTION ***/

/* Dashboard */
Route::get('/admin', 'Backend\SettingsController@index')->name('dashboard.index');

/* Settings */
Route::get('/admin/settings', 'Backend\SettingsController@index')->name('settings.index');
Route::put('/admin/settings', 'Backend\SettingsController@update')->name('settings.update');

/* Categories */
Route::resource('/admin/categories', 'Backend\CategoriesController', ['except' => ['show']]);
Route::get('/admin/categories/autocomplete', 'Backend\CategoriesController@autocomplete')
    ->name('categories.autocomplete');
Route::get('/admin/categories/datatable', 'Backend\CategoriesController@datatable')
    ->name('categories.datatable');

/* Products */
Route::resource('/admin/products', 'Backend\ProductsController', ['except' => ['show']]);
Route::get('/admin/products/datatable', 'Backend\ProductsController@datatable')
    ->name('products.datatable');
Route::get('/admin/products/autocomplete', 'Backend\ProductsController@autocomplete')
    ->name('products.autocomplete');

/* Brands */
Route::resource('/admin/brands', 'Backend\BrandsController', ['except' => ['show']]);
Route::get('/admin/brands/autocomplete', 'Backend\BrandsController@autocomplete')
    ->name('brands.autocomplete');
Route::get('/admin/brands/datatable', 'Backend\BrandsController@datatable')
    ->name('brands.datatable');

/* Banners */
// 2 Words
Route::get('/admin/banner/2words', 'Backend\Module\TwoWordsController@index')
    ->name('banner.2words');
Route::post('/admin/banner/2words', 'Backend\Module\TwoWordsController@save');
// Top 5
Route::get('/admin/banner/top5', 'Backend\Module\Top5Controller@index')
    ->name('banner.top5');
Route::post('/admin/banner/top5', 'Backend\Module\Top5Controller@save');
// Carousel
Route::get('/admin/banner/carousel', 'Backend\Module\CarouselController@index')
    ->name('banner.carousel');
Route::post('/admin/banner/carousel', 'Backend\Module\CarouselController@save');
// Collection
Route::get('/admin/banner/collection', 'Backend\Module\CollectionController@index')
    ->name('banner.collection');
Route::post('/admin/banner/collection', 'Backend\Module\CollectionController@save');

/* Pages */
Route::resource('/admin/pages', 'Backend\PagesController', ['except' => ['show']]);
Route::get('/admin/pages/datatable', 'Backend\PagesController@datatable')
    ->name('pages.datatable');

/* Articles */
Route::resource('/admin/articles', 'Backend\ArticlesController', ['except' => ['show']]);
Route::get('/admin/articles/datatable', 'Backend\ArticlesController@datatable')
    ->name('articles.datatable');
Route::get('/admin/articles/autocomplete', 'Backend\ArticlesController@autocomplete')
    ->name('articles.autocomplete');