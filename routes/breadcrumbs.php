<?php

/*** STORE SECTION ***/

/* Product */
// Sheriff-Shop
Breadcrumbs::register('store', function($breadcrumbs) {
	$breadcrumbs->push('Sheriff-Shop', route('store.product.list'));
});
// Sheriff-Shop > $category > $product OR Sheriff-Shop > $product
Breadcrumbs::register('store.product', function($breadcrumbs, $product, $category = false) {
	if ($category) {
		$breadcrumbs->parent('store.category', $category);
	} else {
		$breadcrumbs->parent('store');
	}
	$breadcrumbs->push($product->description->title, route('store.product', ['product' => $product->id, 'product_url_key' => $product->url_key]));
});

/* Categories */
// Sheriff-Shop > $category
Breadcrumbs::register('store.category', function($breadcrumbs, $category) {
	$breadcrumbs->parent('store');
	$breadcrumbs->push($category->description->title, route('store.category', ['category_url_key' => $category->url_key, 'category' => $category->id]));
});

/* Cart */
Breadcrumbs::register('store.cart', function($breadcrumbs) {
	$breadcrumbs->parent('store');
	$breadcrumbs->push('Корзина', route('store.cart'));
});

/* WishList */
Breadcrumbs::register('store.wishlist', function($breadcrumbs) {
	$breadcrumbs->parent('store');
	$breadcrumbs->push('Избранное', route('store.wishlist'));
});

/* Checkout */
Breadcrumbs::register('store.checkout', function($breadcrumbs) {
	$breadcrumbs->parent('store');
	$breadcrumbs->push('Оформить заказ', route('store.checkout'));
});
Breadcrumbs::register('store.checkout.success', function($breadcrumbs) {
	$breadcrumbs->parent('store.checkout');
	$breadcrumbs->push('Успешно', route('store.checkout.success'));
});

/* Brands */
// Sheriff-Shop > $brand
Breadcrumbs::register('store.brand', function($breadcrumbs, $brand) {
	$breadcrumbs->parent('store');
	$breadcrumbs->push($brand->title, route('store.brand', ['brand_url_key' => $brand->url_key, 'brand' => $brand->id]));
});

/* Articles */
// Sheriff-Shop > Статьи
Breadcrumbs::register('store.article.list', function($breadcrumbs) {
	$breadcrumbs->parent('store');
	$breadcrumbs->push('Статьи', route('store.article.list'));
});
// Sheriff-Shop > Статьи > $article
Breadcrumbs::register('store.article', function($breadcrumbs, $article) {
	$breadcrumbs->parent('store.article.list');
	$breadcrumbs->push($article->description->title, route('store.article', $article->url_key));
});

/*** BACKEND SECTION ***/

/* Dashboard */
// Главная
Breadcrumbs::register('dashboard.index', function($breadcrumbs) {
	$breadcrumbs->push('Главная', route('dashboard.index'));
});

/* Dashboard */
// Настройки
Breadcrumbs::register('settings.index', function($breadcrumbs) {
	$breadcrumbs->push('Настройки', route('settings.index'));
});

/* Categories */
// Главная > Категории
Breadcrumbs::register('categories.index', function($breadcrumbs) {
	$breadcrumbs->parent('dashboard.index');
	$breadcrumbs->push('Категории', route('categories.index'));
});
// Главная > Категории > Добавить
Breadcrumbs::register('categories.create', function($breadcrumbs) {
	$breadcrumbs->parent('categories.index');
	$breadcrumbs->push('Добавить', route('categories.create'));
});
// Главная > Категории > Редактировать
Breadcrumbs::register('categories.edit', function($breadcrumbs, $category) {
	$breadcrumbs->parent('categories.index');
	$breadcrumbs->push('Редактировать', route('categories.edit', $category->id));
});

/* Brands */
// Главная > Бренды
Breadcrumbs::register('brands.index', function($breadcrumbs) {
	$breadcrumbs->parent('dashboard.index');
	$breadcrumbs->push('Бренды', route('brands.index'));
});
// Главная > Бренды > Добавить
Breadcrumbs::register('brands.create', function($breadcrumbs) {
	$breadcrumbs->parent('brands.index');
	$breadcrumbs->push('Добавить', route('brands.create'));
});
// Главная > Бренды > Редактировать
Breadcrumbs::register('brands.edit', function($breadcrumbs, $brand) {
	$breadcrumbs->parent('brands.index');
	$breadcrumbs->push('Редактировать', route('brands.edit', $brand->id));
});

/* Products */
// Главная > Продукты
Breadcrumbs::register('products.index', function($breadcrumbs) {
	$breadcrumbs->parent('dashboard.index');
	$breadcrumbs->push('Товары', route('products.index'));
});
// Главная > Продукты > Добавить
Breadcrumbs::register('products.create', function($breadcrumbs) {
	$breadcrumbs->parent('products.index');
	$breadcrumbs->push('Добавить', route('products.create'));
});
// Главная > Продукты > Редактировать
Breadcrumbs::register('products.edit', function($breadcrumbs, $product) {
	$breadcrumbs->parent('products.index');
	$breadcrumbs->push('Редактировать', route('products.edit', $product->id));
});

/* Banners */
// Главная > 2 Words
Breadcrumbs::register('banner.2words', function($breadcrumbs) {
	$breadcrumbs->parent('dashboard.index');
	$breadcrumbs->push('2 Words', route('banner.2words'));
});
// Главная > Top 5
Breadcrumbs::register('banner.top5', function($breadcrumbs) {
	$breadcrumbs->parent('dashboard.index');
	$breadcrumbs->push('Top 5', route('banner.top5'));
});
// Главная > Carousel
Breadcrumbs::register('banner.carousel', function($breadcrumbs) {
	$breadcrumbs->parent('dashboard.index');
	$breadcrumbs->push('Carousel', route('banner.carousel'));
});
// Главная > Collection
Breadcrumbs::register('banner.collection', function($breadcrumbs) {
	$breadcrumbs->parent('dashboard.index');
	$breadcrumbs->push('Collection', route('banner.collection'));
});

/* Pages */
// Главная > Страница
Breadcrumbs::register('pages.index', function($breadcrumbs) {
	$breadcrumbs->parent('dashboard.index');
	$breadcrumbs->push('Страницы', route('pages.index'));
});
// Главная > Страница > Добавить
Breadcrumbs::register('pages.create', function($breadcrumbs) {
	$breadcrumbs->parent('pages.index');
	$breadcrumbs->push('Добавить', route('pages.create'));
});
// Главная > Страница > Редактировать
Breadcrumbs::register('pages.edit', function($breadcrumbs, $product) {
	$breadcrumbs->parent('pages.index');
	$breadcrumbs->push('Редактировать', route('pages.edit', $product->id));
});

/* Articles */
// Главная > Статьи
Breadcrumbs::register('articles.index', function($breadcrumbs) {
	$breadcrumbs->parent('dashboard.index');
	$breadcrumbs->push('Статьи', route('articles.index'));
});
// Главная > Статьи > Добавить
Breadcrumbs::register('articles.create', function($breadcrumbs) {
	$breadcrumbs->parent('articles.index');
	$breadcrumbs->push('Добавить', route('articles.create'));
});
// Главная > Статьи > Редактировать
Breadcrumbs::register('articles.edit', function($breadcrumbs, $product) {
	$breadcrumbs->parent('articles.index');
	$breadcrumbs->push('Редактировать', route('articles.edit', $product->id));
});
