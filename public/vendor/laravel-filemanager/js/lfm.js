(function( $ ){

  $.fn.filemanager = function(type, options) {
    type = type || 'file';

    this.on('click', function(e) {
      var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
      localStorage.setItem('target_input', $(this).data('input'));
      localStorage.setItem('target_preview', $(this).data('preview'));

      if (options.modal) {
        var $iframe = $('#' + options.modal.iframe);
        
        if ($iframe.attr('src')) {
          $('#' + options.modal.dialog).modal('show');  
        } else {
          $iframe.attr('src', route_prefix + '?modal=' + options.modal.dialog + '&type=' + type);
        }

      } else {
        window.open(route_prefix + '?type=' + type, 'FileManager', 'width=900,height=600');
      }
      
      return false;
    });
  }

})(jQuery);


function SetUrl(url, file_path){
  //set the value of the desired input to image url
  var target_input = $('#' + localStorage.getItem('target_input'));
  target_input.val(file_path);

  //set or change the preview image src
  var target_preview = $('#' + localStorage.getItem('target_preview'));
  target_preview.attr('src', url);
}