function autocomplete(selector, url) {
    selector.select2({
        placeholder: " -- Нет -- ",
        allowClear: true,
        theme: "bootstrap",
        minimumInputLength: 1,
        language: "ru",
        ajax: {
            url: url,
            dataType: 'json',
            delay: 250,
            data: function(params) {
                return {
                    term: params.term || "",
                    page: params.page || 1
                }
            },
            processResults: function (data) {
                return {
                    results: $.map(data.results, function (item) {
                        return {
                            text: item.title,
                            id: item.id
                        }
                    }),
                    pagination: {
                        more: data.pagination.more
                    }
                };
            },
            cache: true,
        }
    });
}