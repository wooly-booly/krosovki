<?php
namespace WBstore;

use Franzose\ClosureTable\Models\Entity;
use Illuminate\Support\Facades\DB;

class Category extends Entity implements CategoryInterface
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    protected $fillable = [
        'url_key', 'image', 'parent_id', 'position', 'status'
    ];

    /**
     * ClosureTable model instance.
     *
     * @var categoryClosure
     */
    protected $closure = 'WBstore\CategoryClosure';

    public function parent()
    {
        return $this->belongsTo('WBstore\Category', 'parent_id');
    }

    public function description()
    {
        return $this->hasOne('WBstore\CategoryDescription');
    }

	public function products()
	{
		return $this->belongsToMany('WBstore\Product', 'product_category');
	}

    /**
     * Return categories in format "category > subcategrory > ..."
     */
	public static function formattedCategories()
	{
		$category = new static;
		$closureTable = $category->closure->getTable();

		// get categories in format: category > subcategrory > ...
		$categories = DB::table($closureTable . ' as cp')
			->select(
				'cp.descendant AS id',
				'c1.parent_id',
				'c1.position',
				DB::raw('GROUP_CONCAT(cd2.title ORDER BY cp.depth DESC SEPARATOR "&nbsp;&nbsp;&gt;&nbsp;&nbsp;") AS title')
			)
			->leftJoin($category->table . ' as c1', 'cp.descendant', '=', 'c1.id')
			->leftJoin($category->table . ' as c2', 'cp.ancestor', '=', 'c2.id')
            ->leftJoin('category_description' . ' as cd2', 'cd2.category_id', '=', 'c2.id')
			->groupBy('cp.descendant');
			// ->orderBy('title', 'asc')
			// ->get();

		return $categories;
	}

    public function setParentIdAttribute($value)
    {
        $this->attributes['parent_id'] = $value ?: null;
    }

    public function setUrlKeyAttribute($value)
    {
        $this->attributes['url_key'] = $value ?: null;
    }
}
