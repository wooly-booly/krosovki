<?php

namespace WBstore;

use Illuminate\Database\Eloquent\Model;

class ProductDescription extends Model
{
    public $timestamps = false;
    protected $table = 'product_description';

    protected $fillable = [
        'title', 'description', 'meta_description', 'meta_keyword'
    ];

    public function product()
    {
        return $this->belongsTo('WBstore\Product');
    }
}
