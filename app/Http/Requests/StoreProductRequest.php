<?php

namespace WBstore\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand_id' => 'integer',
            'url_key' => 'regex:/^[0-9\-a-zA-Zа-яА-Я_]+$/|unique:products,url_key',
            // 'image' => 'image|mimes:jpeg,jpg,bmp,png,gif',
			'price' => 'required|numeric',
            'position' => 'integer',
            'status' => 'integer',
            // 'cateogry_ids' => 'integer',
            'description.title' => 'required|min:2',
            'description.meta_description' => 'max:255',
            'description.meta_keyword' => 'max:255',
        ];
    }

    public function withValidator($validator)
    {
        if ($validator->fails()) {
            session()->flash('error', "Что-то пошло не так! Проверьте форму на наличие ошибок.");
        }
    }

}
