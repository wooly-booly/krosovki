<?php

namespace WBstore\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'position' => 'integer',
            'status' => 'integer',
            'url_key' => 'required|regex:/^[0-9\-a-zA-Zа-яА-Я_]+$/|unique:pages,url_key',
            'description.title' => 'required|min:2',
            'description.content' => 'required',
            'description.meta_description' => 'max:255',
            'description.meta_keyword' => 'max:255',
        ];
    }
}
