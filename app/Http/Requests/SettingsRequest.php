<?php

namespace WBstore\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "config.store_title" => 'required',
            // "config.address" => '',
            "config.email" => 'required|email',
            // "config.comment" => '',
            // "config.open_hours" => '',
            "config.phone" => 'required',
            "config.meta_title" => 'required',
            "config.meta_description" => 'max:255',
            "config.meta_keyword" => 'max:255',
            "config.logo" => 'required',
            // "config.icon" => ''
        ];
    }
}
