<?php

namespace WBstore\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'published_at' => 'date_format:Y-m-d H:i:s',
            'url_key' => 'required|regex:/^[0-9\-a-zA-Zа-яА-Я_]+$/|unique:pages,url_key',
            'image' => 'required',
            'description.title' => 'required|min:2',
            'description.body' => 'required',
            'description.meta_description' => 'max:255',
            'description.meta_keyword' => 'max:255'
        ];
    }
}
