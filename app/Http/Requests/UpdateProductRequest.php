<?php

namespace WBstore\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			'category_id' => 'required|integer',
			'title' => 'required|min:2',
			'description' => 'required|min:10',
			'price' => 'required|numeric',
			'status' => 'integer',
			// 'image' => 'required|image|mimes:jpeg,jpg,bmp,png,gif',
        ];
    }
}
