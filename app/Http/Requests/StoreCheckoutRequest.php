<?php

namespace WBstore\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCheckoutRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'firstname' => 'required|max:40',
            'lastname' => 'required|max:40',
            'email' => 'required|email',
            'phone' => 'required|max:25',
            'city' => 'required|max:60',
            'address' => 'required',
            'comment' => 'min:2',
        ];
    }
}
