<?php

namespace WBstore\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url_key' => 'regex:/^[0-9\-a-zA-Zа-яА-Я_]+$/|unique:categories,url_key',
            'position' => 'integer',
            'parent_id' => 'integer',
            'status' => 'integer',
            // 'image' => '',
            'description.title' => 'required|min:2',
            'description.meta_description' => 'max:255',
            'description.meta_keyword' => 'max:255',
        ];
    }
}
