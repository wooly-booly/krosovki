<?php

namespace WBstore\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'url_key' => 'regex:/^[0-9\-a-zA-Zа-яА-Я_]+$/',
            // 'image' => '',
            'position' => 'integer',
        ];
    }
}
