<?php

namespace WBstore\Http\Requests\Banner;

use Illuminate\Foundation\Http\FormRequest;

class CollectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'setting.title_left' => 'required',
            'setting.description_left' => 'required',
            'setting.title_right' => 'required',
            'setting.description_right' => 'required',
            'banner.*.image' => 'required',
            'banner.*.alt' => 'required',
            'banner.*.link' => 'required|url',
            'banner.*.position' => 'required|integer',
        ];
    }

    public function withValidator($validator)
    {
        if ($validator->fails()) {
            session()->flash('error', "Что-то пошло не так! Все поля должны быть заполнены.");
        }
    }
}
