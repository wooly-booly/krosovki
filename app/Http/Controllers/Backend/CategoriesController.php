<?php

namespace WBstore\Http\Controllers\Backend;

use Illuminate\Http\Request;
use WBstore\Http\Requests\StoreCategoryRequest;
use WBstore\Http\Requests\UpdateCategoryRequest;
use Yajra\Datatables\Datatables;
use WBstore\Category;
use WBstore\CategoryDescription;
use Illuminate\Support\Facades\DB;
use Session;
use Image;
use File;

class CategoriesController extends Controller
{
    public function index()
    {
		return view('backend.categories.index');
    }

    public function create(Category $category)
    {
		return view('backend.categories.create')
            ->with('category', $category);
    }

    public function store(StoreCategoryRequest $request, Category $category)
    {
        $data = $request->all();

        DB::transaction(function() use ($category, $data) {
    		$newCategory = $category->create($data);
            $newCategory->description()->create($data['description']);
        });

		Session::flash('success', "Категория: '" . $data['description']['title'] . "' успешно добавлена!");

		return redirect()->route('categories.index');
    }

    public function edit(Category $category)
    {
        $category = $category->with('parent')->find($category->id);
        
		return view('backend.categories.edit', compact('category'));
    }

    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $data = $request->all();

        if (!isset($data['parent_id'])) {
            $data['parent_id'] = null;
        }

        DB::transaction(function() use ($category, $data) {
    		$category->update($data);
            $category->description()->update($data['description']);
        });

		Session::flash('success', "Категория: '" . $category->title . "' обновлена!");

		return redirect()->route('categories.index');
    }

    public function destroy(Category $category)
    {
        if ($category->exists) {
            if ($category->image) {
                File::delete($category->image);
            }
            $category->delete();
        }
    }

    public function autocomplete(Request $request)
    {
        if (request()->ajax()) {
            $data = [];

            if ($request->has('term', 'page')) {

                $page = $request->page;
                $resultCount = 10;
                $offset = ($page - 1) * $resultCount;

                $search = $request->term;

                $data = CategoryDescription::select("category_id as id", "title")->where('title', 'LIKE', "%$search%")->orderBy('title')->skip($offset)->take($resultCount)->get();

                $count = CategoryDescription::where('title', 'LIKE', "%$search%")->count();
                $endCount = $offset + $resultCount;
                $morePages = $count > $endCount;

                $results = array(
                    "results" => $data,
                    "pagination" => array(
                        "more" => $morePages
                    )
                );
            }

            return response()->json($results);
        }
    }

    public function datatable()
    {
        if (request()->ajax()) {
            return Datatables::of(Category::formattedCategories())
                ->filter(function ($query) {
                    if (!empty(request('search')['value'])) {
                        $query->where('cd.title', 'like', "%" . request('search')['value'] . "%");
                    }
                })
                ->make(true);
        }
    }

}
