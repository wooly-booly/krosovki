<?php

namespace WBstore\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Session;
use Yajra\Datatables\Datatables;
use WBstore\Http\Requests\StoreBrandRequest;
use WBstore\Http\Requests\UpdateBrandRequest;
use WBstore\Brand;

class BrandsController extends Controller
{
    public function index()
    {
        return view('backend.brands.index');
    }

    public function create(Brand $brand)
    {
        return view('backend.brands.create')
            ->with('brand', $brand);
    }

    public function store(StoreBrandRequest $request, Brand $brand)
    {
        $data = $request->all();
        $brand = $brand->create($data);

        return redirect()->route('brands.index')
            ->with('success', 'Бренд "' . $data['title'] . '" успешно добавлен!');
    }

    public function edit(Brand $brand)
    {
        return view('backend.brands.edit', compact('brand'));
    }

    public function update(UpdateBrandRequest $request, Brand $brand)
    {
        $data = $request->all();

        $brand->update($data);

        Session::flash('success', "Информация и бренде '" . $data['title'] . "' обновлена!");

        return back();
        
    }

    public function destroy(Brand $brand)
    {
        if ($brand->exists) {
            if ($brand->image) {
                File::delete($brand->image);
            }
            $brand->delete();
        }
    }

    public function autocomplete(Request $request)
    {
        if (request()->ajax()) {           
            $data = [];

            if ($request->has('term', 'page')) {

                $page = $request->page;
                $resultCount = 10;
                $offset = ($page - 1) * $resultCount;

                $search = $request->term;

                $data = Brand::select("id", "title")->where('title', 'LIKE', "%$search%")->orderBy('title')->skip($offset)->take($resultCount)->get();

                $count = Brand::where('title', 'LIKE', "%$search%")->count();
                $endCount = $offset + $resultCount;
                $morePages = $count > $endCount;

                $results = array(
                    "results" => $data,
                    "pagination" => array(
                        "more" => $morePages
                    )
                );
            }

            return response()->json($results);
        }
    }

    public function datatable()
    {
        if (request()->ajax()) {
            return Datatables::of(Brand::query())
                ->filter(function ($query) {
                    if (!empty(request('search')['value'])) {
                        $query->where('title', 'like', "%" . request('search')['value'] . "%");
                    }
                })
                ->make(true);
        }
    }

}
