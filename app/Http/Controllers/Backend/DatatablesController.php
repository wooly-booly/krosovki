<?php

namespace WBstore\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use WBstore\Category;

class DatatablesController extends Controller
{
    public function categories()
    {
        if (request()->ajax()) {
            return Datatables::of(Category::formattedCategories())
                ->filter(function ($query) {
                    if (!empty(request('search')['value'])) {
                        $query->where('cd.title', 'like', "%" . request('search')['value'] . "%");
                    }
                })
                ->make(true);
        }
    }
}
