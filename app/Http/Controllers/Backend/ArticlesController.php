<?php

namespace WBstore\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use WBstore\Http\Requests\StoreArticleRequest;
use WBstore\Http\Requests\UpdateArticleRequest;
use WBstore\Article;
use WBstore\ArticleDescription;
use Carbon\Carbon;

class ArticlesController extends Controller
{
    public function index(Article $articles)
    {
        return view('backend.articles.index')->with('articles', $articles->paginate(10));
    }

    public function create(Article $article)
    {
        return view('backend.articles.create', compact('article'));
    }

    public function store(StoreArticleRequest $request, article $article)
    {
        $data = $request->all();

        DB::transaction(function() use ($article, $data) {
            $article = $article->create($data);
            $article->description()->create($data['description']);

            if (!empty($data['related_articles'])) {
                $article->relatedArticles()->attach($data['related_articles']);  
            }

            if (!empty($data['related_products'])) {
                $article->relatedProducts()->attach($data['related_products']);  
            }
        });

        return redirect()->route('articles.index')
            ->with('success', 'Статья "'. $data['description']['title'] .'"  успешно сохранена!');
    }

    public function edit(Article $article)
    {
        $article->relatedArticlesById = $article->relatedArticles->pluck('description.title', 'id')->all();
        $article->relatedProductsById = $article->relatedProducts->pluck('description.title', 'id')->all();

        return view('backend.articles.edit', compact('article'));
    }

    public function update(UpdateArticleRequest $request, article $article)
    {
        $data = $request->all();

        DB::transaction(function() use ($article, $data) {
            $article->update($data);
            $article->description()->update($data['description']);

            $article->relatedArticles()->detach();
            if (!empty($data['related_articles'])) {
                $article->relatedArticles()->attach($data['related_articles']);  
            }

            $article->relatedProducts()->detach();
            if (!empty($data['related_products'])) {
                $article->relatedProducts()->attach($data['related_products']);  
            }
        });

        return redirect()->route('articles.index')
            ->with('success', 'Статья "'. $data['description']['title'] .'"  успешно обновлена!');
    }

    public function confirm(Article $article)
    {
        return view('backend.articles.confirm', compact('article'));
    }

    public function destroy(Article $article)
    {
        if ($article) {
            $article->delete();
        }
    }

    public function datatable()
    {
        if (request()->ajax()) {
            return Datatables::of(Article::with(['description' => function($q) {
                        $q->select('article_id', 'title');
                }]))
                ->filter(function ($query) {
                    if (!empty(request('search')['value'])) {
                        $query->whereHas('description', function($q) {
                            $q->where('title', 'like', "%" . request('search')['value'] . "%");
                        });
                    }
                })
                ->editColumn('published_at', function($aritcle) {
                    $now = Carbon::now();

                    if ($aritcle->published_at < $now) {
                        return $aritcle->present()->published_date;
                    } elseif ($aritcle->published_at > $now) {
                        return 'Ожидает: ' . $aritcle->present()->published_date('%d %B %Y, %I:%M');
                    }

                    return 'Не опупликован';
                })
                ->make(true);
        }
    }

    public function autocomplete(Request $request)
    {
        if (request()->ajax()) {
            $data = [];

            if ($request->has('term', 'page')) {

                $page = $request->page;
                $resultCount = 10;
                $offset = ($page - 1) * $resultCount;

                $search = $request->term;

                $data = ArticleDescription::select("article_id as id", "title")->where('title', 'LIKE', "%$search%")->orderBy('title')->skip($offset)->take($resultCount)->get();

                $count = ArticleDescription::where('title', 'LIKE', "%$search%")->count();
                $endCount = $offset + $resultCount;
                $morePages = $count > $endCount;

                $results = array(
                    "results" => $data,
                    "pagination" => array(
                        "more" => $morePages
                    )
                );
            }

            return response()->json($results);
        }
    }
}
