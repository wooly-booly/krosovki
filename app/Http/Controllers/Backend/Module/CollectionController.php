<?php

namespace WBstore\Http\Controllers\Backend\Module;

use WBstore\Http\Requests\Banner\CollectionRequest;
use WBstore\Http\Controllers\Backend\Controller;
use Illuminate\Support\Facades\DB;
use WBstore\Setting;
use WBstore\Banner;
use Validator;

class CollectionController extends Controller
{
    private $banner;
    public $title = 'Collection';
    public $code = 'collection';

    public function __construct()
    {
        parent::__construct();
        $this->banner = Banner::with('images')->where('title', $this->title)->first();
    }

    public function index()
    {
        $setting = Setting::get($this->code);

        return view('backend.module.banner.collection')
            ->with('banner', $this->banner->images)
            ->with('setting', $setting);
    }

    public function save(CollectionRequest $request)
    {
        $data = $request->all();

        DB::transaction(function() use ($data) {
            $this->banner->images()->delete();

            if (!empty($data['banner'])) {
                foreach ($data['banner'] as $imgInfo) {
                    $this->banner->images()->create($imgInfo);  
                }
            }

            $this->setSettings($data['setting']);
        });

        return redirect()->route('banner.collection')->with('success', 'Информация успешно сохранена!');
    }

    private function setSettings($settings)
    {
        Setting::where('code', $this->code)->delete();

        $fillable = ['title_left', 'title_right', 'description_left', 'description_right'];

        foreach($settings as $key => $value) {
            if (in_array($key, $fillable)) {
                Setting::create([
                    'code' => $this->code,
                    'key' => $key,
                    'value' => $value,
                    'serialized' => 0
                ]); 
            }
        }

        return $settings;
    }
}