<?php

namespace WBstore\Http\Controllers\Backend\Module;

use Illuminate\Http\Request;
use WBstore\Http\Controllers\Backend\Controller;
use WBstore\Banner;

class TwoWordsController extends Controller
{
    private $title = '2 words';

    public function index()
    {
        $data['banner'] = $this->banner()->images->first();

        return view('backend.module.banner.2words', $data);
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'image' => 'required',
            'alt' => 'required',
            'link' => 'required|url',
        ]);

        $data = $request->all();
        $banner = $this->banner();
        
        $banner->images()->delete();
        $banner->images()->create($data);  

        return redirect()->route('banner.2words')->with('success', 'Информация успешно сохранена!');
    }

    private function banner()
    {
        return Banner::get($this->title);
    }
}
