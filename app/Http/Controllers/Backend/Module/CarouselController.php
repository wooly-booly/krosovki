<?php

namespace WBstore\Http\Controllers\Backend\Module;

use Illuminate\Http\Request;
use WBstore\Http\Controllers\Backend\Controller;
use WBstore\Banner;
use Validator;

class CarouselController extends Controller
{
    private $title = 'Carousel';

    public function index()
    {
        $data['banner'] = $this->banner();

        return view('backend.module.banner.carousel', $data);
    }

    public function save(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'banner.*.image' => 'required',
            'banner.*.alt' => 'required',
            'banner.*.link' => 'required|url',
            'banner.*.position' => 'integer',
        ]);

        if ($validator->fails()) {
            return redirect()->route('banner.carousel')->with('error', "Что-то пошло не так! Проверьте записи на наличие ошибок.");
        }

        $banner = $this->banner();
        $banner->images()->delete();

        if (!empty($data['banner'])) {
            foreach ($data['banner'] as $info) {
                $banner->images()->create($info);  
            }
        }

        return redirect()->route('banner.carousel')->with('success', 'Информация успешно сохранена!');
    }

    private function banner()
    {
        return Banner::get($this->title);
    }
}
