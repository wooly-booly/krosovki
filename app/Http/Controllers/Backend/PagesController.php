<?php

namespace WBstore\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use WBstore\Http\Requests\StorePageRequest;
use WBstore\Http\Requests\UpdatePageRequest;
use WBstore\Page;

class PagesController extends Controller
{
    protected $pages;

    public function __construct(Page $pages)
    {
        $this->pages = $pages;
        parent::__construct();
    }

    public function index()
    {
        $pages = $this->pages->paginate(10);

        return view('backend.pages.index', compact('pages'));
    }

    public function create(Page $page)
    {
        return view('backend.pages.create', compact('page'));
    }

    public function store(StorePageRequest $request, Page $page)
    {
        $data = $request->all();

        DB::transaction(function() use ($page, $data) {
            $page = $page->create($data);
            $page->description()->create($data['description']);
        });

        return redirect()->route('pages.index')
            ->with('success', 'Страница "'. $data['description']['title'] .'"  успешно создана!');
    }

    public function edit(Page $page)
    {
        return view('backend.pages.edit', compact('page'));
    }

    public function update(UpdatePageRequest $request, Page $page)
    {
        $data = $request->all();

        DB::transaction(function() use ($page, $data) {
            $page->update($data);
            $page->description()->update($data['description']);
        });

        return redirect()->route('pages.index')
            ->with('success', 'Страница "'. $data['description']['title'] .'"  успешно обновлена!');
    }

    public function confirm($id)
    {
        $page = $this->pages->findOrFail($id);

        return view('backend.pages.confirm', compact('page'));
    }

    public function destroy(Page $page)
    {
        if ($page) {
            $page->delete();
        }
    }

    public function datatable()
    {
        if (request()->ajax()) {
            return Datatables::of(Page::with('description'))
                ->filter(function ($query) {
                    if (!empty(request('search')['value'])) {
                        $query
                            ->leftJoin('page_description as pd', 'pd.page_id', '=', 'pages.id')
                            ->where('pd.title', 'like', "%" . request('search')['value'] . "%");
                    }
                })
                ->editColumn('status', function($product) {
                    if ($product->status == 1) {
                        return 'Включено';
                    }

                    return 'Отключено';
                })
                ->make(true);
        }
    }
}
