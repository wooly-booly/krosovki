<?php

namespace WBstore\Http\Controllers\Backend;

use Illuminate\Http\Request;
use WBstore\Http\Requests\SettingsRequest;
use WBstore\Setting;

class SettingsController extends Controller
{
    public function index()
    {
        $config = Setting::select('key', 'value')->where('code', '=', Setting::$code)->get();
        $settings = collect();

        foreach ($config as $item) {
            $settings->{Setting::$code}[$item->key] = $item->value;
        }

        return view('backend.settings.index', compact('settings'));
    }

    public function update(SettingsRequest $request)
    {
        Setting::where('code', '=', Setting::$code)->delete();

        $data = $request->all();
        $config = [];

        foreach ($data['config'] as $key => $value) {
            $config[] = ['code' => Setting::$code, 'key' => $key, 'value' => $value];
        }
    
        Setting::insert($config);

        return redirect()->route('settings.index')->with('success', 'Настройки сохранены!');
    }
}