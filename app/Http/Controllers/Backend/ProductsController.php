<?php

namespace WBstore\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use WBstore\Http\Requests\StoreProductRequest;
use WBstore\Http\Requests\UpdateProductRequest;
use Yajra\Datatables\Datatables;
use WBstore\Product;
use WBstore\Category;
use WBstore\ProductDescription;
use Image;
use File;

class ProductsController extends Controller
{
    public function index()
    {
		return view('backend.products.index');
    }

    public function create(Product $product)
    {
		return view('backend.products.create')
			->with('product', $product);
    }

    public function store(StoreProductRequest $request, Product $product)
    {
    	$data = $request->all();

        DB::transaction(function() use ($product, $data) {
    		$product = $product->create($data);
            $product->description()->create($data['description']);

            if (!empty($data['category_ids'])) {
                $product->categories()->attach($data['category_ids']);  
            }

            if (!empty($data['product_image'])) {
                foreach ($data['product_image'] as $img) {
                    $product->images()->create($img);  
                }
            }
        });

		return redirect()->route('products.index')
			->with('success', 'Товар "' . $data['description']['title'] . '" успешно добавлен!');
    }

    public function edit(Product $product)
    {
        $product->categoriesById = $product->categories->pluck('description.title', 'id')->all();

        return view('backend.products.edit')
            ->with('product', $product)
            ->with('products', Product::all()); 
    }

    public function update(Request $request, Product $product)
    {
        $data = $request->all();

        DB::transaction(function() use ($product, $data) {
            $product->update($data);
            $product->description()->update($data['description']);
            
            $product->categories()->detach();
            if (!empty($data['category_ids'])) {
                $product->categories()->attach($data['category_ids']);  
            }

            $product->images()->delete();
            if (!empty($data['product_image'])) {
                foreach ($data['product_image'] as $img) {
                    $product->images()->create($img);  
                }
            }
        });

        return redirect()->route('products.index')
            ->with('success', 'Информация о товаре "' . $data['description']['title'] . '" успешно обновлена!');
    }

    public function destroy(Product $product)
    {
        if ($product) {
            // Get paths to product images
            $images   = $product->images()->get()->pluck('image');
            $images[] = $product->image;

            DB::transaction(function() use ($product) {
                $product->categories()->detach();
                $product->images()->delete();
                $product->delete();
            });

            foreach($images as $img) {
                File::delete(ltrim($img, '/'));
            }
        }
    }

    public function datatable()
    {
        if (request()->ajax()) {
            return Datatables::of(Product::with('description'))
                ->filter(function ($query) {
                    if (!empty(request('search')['value'])) {
                        $query
                            ->leftJoin('product_description as pd', 'pd.product_id', '=', 'products.id')
                            ->where('pd.title', 'like', "%" . request('search')['value'] . "%");
                    }
                })
                ->editColumn('status', function($product) {
                    if ($product->status == 1) {
                        return 'Включено';
                    }

                    return 'Отключено';
                })
                ->editColumn('image', function($product) {
                    if ($product->image) {
                        return $product->present()->thumb;
                    }
                })
                ->make(true);
        }
    }

    public function autocomplete(Request $request)
    {
        if (request()->ajax()) {
            $data = [];

            if ($request->has('term', 'page')) {

                $page = $request->page;
                $resultCount = 10;
                $offset = ($page - 1) * $resultCount;

                $search = $request->term;

                $data = ProductDescription::select("product_id as id", "title")->where('title', 'LIKE', "%$search%")->orderBy('title')->skip($offset)->take($resultCount)->get();

                $count = ProductDescription::where('title', 'LIKE', "%$search%")->count();
                $endCount = $offset + $resultCount;
                $morePages = $count > $endCount;

                $results = array(
                    "results" => $data,
                    "pagination" => array(
                        "more" => $morePages
                    )
                );
            }

            return response()->json($results);
        }
    }
}
