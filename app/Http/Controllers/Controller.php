<?php

namespace WBstore\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use \WBstore\Services\Menu\Category as CategoryMenu;
use \WBstore\Services\Menu\Cart as CartMenu;
use \WBstore\Services\Menu\Customer as CustomerMenu;
use Breadcrumbs;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function __construct()
	{
        Breadcrumbs::setView('store.partials.breadcrumbs');

		view()->share('category_menu', new CategoryMenu);
		view()->share('cart_menu', new CartMenu);
		view()->share('customer_menu', new CustomerMenu);
	}
}
