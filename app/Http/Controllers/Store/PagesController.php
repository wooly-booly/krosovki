<?php

namespace WBstore\Http\Controllers\Store;

use Illuminate\Http\Request;
use WBstore\Http\Controllers\Controller;

class PagesController extends Controller
{
	public function home()
	{
		return 'home';
	}

	public function blog()
	{
		return 'blog';
	}
	public function about()
	{
		return 'about';
	}

	public function contact()
	{
		return 'contact';
	}

}
