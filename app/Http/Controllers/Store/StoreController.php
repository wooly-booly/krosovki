<?php

namespace WBstore\Http\Controllers\Store;

use Illuminate\Http\Request;
use WBstore\Http\Controllers\Controller;
use WBstore\Product;
use WBstore\Article;
use Carbon\Carbon;

class StoreController extends Controller
{
    public function index()
    {
        $articles = Article::take(8)
            ->with(['description' => function($q) {
                    $q->select('article_id', 'title', 'excerpt');
            }])
            ->where('published_at', '<', Carbon::now())
            ->orderBy('published_at', 'desc')
            ->get();

// dd($articles);

        return view('store.common.home')
            ->with('newProducts', Product::take(8)->orderBy('created_at', 'DESC')->get())
            ->with('articles', $articles);
    }
}
