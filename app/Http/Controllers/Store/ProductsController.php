<?php

namespace WBstore\Http\Controllers\Store;

use Illuminate\Http\Request;
use WBstore\Http\Controllers\Controller;
use WBstore\Product;
use WBstore\Category;

class ProductsController extends Controller
{
    public function productList(Product $products)
    {
        $data['page_title'] = 'Список Товаров';
        $data['breadcrumbs'] = (object) ['route' => 'store'];
        $data['products'] = $products->with(['description' => function($q) {
                $q->select('product_id', 'title');
        }])->where('status', 1)->orderBy('created_at', 'DESC')->paginate(9);

        return view('store.products.product_list', $data);
    }

    public function singleProduct(Product $product, Request $request)
    {
        $data['page_title'] = $product->description->title;
        $data['product'] = $product;

        if ($categoryId = $request->cat) {
            $data['category'] = Category::find($categoryId);
        }

        return view('store.products.product_single', $data);
    }

    public function quickView(Product $product)
    {      
        if (request()->ajax() && $product->exists) {
            return view('store.products.quickview', compact("product"));
        }
    }
}
