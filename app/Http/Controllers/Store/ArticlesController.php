<?php

namespace WBstore\Http\Controllers\Store;

use Illuminate\Http\Request;
use WBstore\Http\Controllers\Controller;
use WBstore\Article;
use Carbon\Carbon;

class ArticlesController extends Controller
{
    public function articleList(Article $articles)
    {
        $data['page_title'] = 'Статьи';

        $data['articles'] = $articles
                            // ->with('author')
                            ->with(['description' => function($q) {
                                    $q->select('article_id', 'title', 'excerpt');
                            }])
                            ->where('published_at', '<', Carbon::now())
                            ->orderBy('published_at', 'desc')
                            ->paginate(12);

       return view('store.articles.list', $data);
    }

    public function singleArticle(Article $article, Request $request)
    {
        $article->viewed++;
        $article->save();

        $data['page_title'] = $article->description->title;
        $data['article'] = $article;
        $data['related_articles'] = $article->relatedArticles;
        $data['related_products'] = $article->relatedProducts;

        return view('store.articles.single', $data);
    }
}