<?php

namespace WBstore\Http\Controllers\Store;

use Illuminate\Http\Request;
use WBstore\Http\Controllers\Controller;
use WBstore\Product;
use Cart;

class WishListController extends Controller
{
    public function wishlist()
    {
        $data['page_title'] = 'Избранное';
        $data['products'] = Cart::instance('wishlist')->content();

        return view('store.account.wishlist', $data);
    }

    public function add(Product $product)
    {
        Cart::instance('wishlist')->add([
            'id' => $product->id,
            'name' => $product->description->title,
            'price' => $product->price,
            'qty' => '1',
            'options' => [ 
                'image' => $product->image,
                'brand' => $product->brand ? $product->brand->title : '',
            ]
        ]);
    }

    public function remove($itemId)
    {
        if (request()->ajax()) {
            return Cart::instance('wishlist')->remove($itemId);
        }
    }
}
