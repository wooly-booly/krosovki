<?php

namespace WBstore\Http\Controllers\Store;

use Illuminate\Http\Request;
use WBstore\Http\Controllers\Controller;
use WBstore\Brand;
use WBstore\Product;

class BrandsController extends Controller
{
    public function brand(Brand $brand, Product $products)
    {
        $data['page_title'] = $brand->title;
        $data['breadcrumbs'] = (object) ['route' => 'store.brand', 'option' => $brand];
        $data['products'] = $products->with(['description' => function($q) {
                $q->select('product_id', 'title');
        }])->where('brand_id', $brand->id)
            ->where('status', 1)->orderBy('created_at', 'DESC')->paginate(9);

        return view('store.products.product_list', $data);
    }
}
