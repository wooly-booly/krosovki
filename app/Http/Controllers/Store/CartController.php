<?php

namespace WBstore\Http\Controllers\Store;

use Illuminate\Http\Request;
use WBstore\Http\Controllers\Controller;
use WBstore\Product;
use Cart;

class CartController extends Controller
{
    public function cart()
    {
        $data['page_title'] = 'Корзина';
        $data['products'] = Cart::instance('shopping')->content();

        return view('store.checkout.cart', $data);
    }

    public function add(Product $product)
    {
        if (request()->ajax()) {
            Cart::instance('shopping')->add([
                'id' => $product->id,
                'name' => $product->description->title,
                'price' => $product->price,
    // replace qty
                'qty' => 1,
                'options' => [ 
                    'image' => $product->image,
                    'brand' => $product->brand ? $product->brand->title : '',
    // replace options
                    'size' => 'large',
                    'color' => 'black'
                ]
            ]);

            return response()->json($this->getShortInfo());
        }
    }

    public function update($itemId)
    {
        if (request()->ajax() && request()->has('quantity')) {
            Cart::instance('shopping')->update($itemId, request()->input('quantity'));
            
            return response()->json($this->getShortInfo());
        }
    }

    public function remove($itemId)
    {
        if (request()->ajax()) {
            Cart::instance('shopping')->remove($itemId);
            
            return response()->json($this->getShortInfo());
        }
    }

    public function info()
    {
        if (request()->ajax()) {
            $data['cart'] = Cart::instance('shopping');

            return view('store.partials.cart', $data);
        }
        // else {
        //     Cart::instance('shopping')->destroy();
        // }
    }

    private function getShortInfo()
    {
        return [
            'total' => Cart::instance('shopping')->total(),
            'count' => Cart::instance('shopping')->count(),
        ];
    }
}
