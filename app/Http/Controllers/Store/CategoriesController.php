<?php

namespace WBstore\Http\Controllers\Store;

use Illuminate\Http\Request;
use WBstore\Http\Controllers\Controller;
use WBstore\Category;
use WBstore\Product;

class CategoriesController extends Controller
{
    public function index(Category $category)
    {
        $data['page_title'] = $category->description->title;
        $data['category'] = $category;
        $data['breadcrumbs'] = (object) ['route' => 'store.category', 'option' => $category];
        $data['products'] = $category->products()->with(['description' => function($q) {
            $q->select('product_id', 'title');
        }])->where('status', 1)->orderBy('created_at', 'DESC')->paginate(9);

        return view('store.products.product_list', $data);
    }
}
