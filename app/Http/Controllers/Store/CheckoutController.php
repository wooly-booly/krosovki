<?php

namespace WBstore\Http\Controllers\Store;

use Illuminate\Http\Request;
use WBstore\Http\Controllers\Controller;
use WBstore\Http\Requests\StoreCheckoutRequest;
use Cart;

class CheckoutController extends Controller
{
    public function checkout()
    {
        $data['page_title'] = 'Оформить Заказ';
        $data['cart'] = Cart::instance('shopping');

        return view('store.checkout.checkout', $data);
    }

    public function confirm(StoreCheckoutRequest $request)
    {
        return redirect()->route('store.checkout.success');
    }

    public function success()
    {
        $data['page_title'] = 'Удачных покупок';

        return view('store.checkout.success', $data);
    }
}
