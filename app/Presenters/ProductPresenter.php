<?php
namespace WBstore\Presenters;

use Config;
use Laracasts\Presenter\Presenter;

class ProductPresenter extends Presenter
{
    use Thumb;
}