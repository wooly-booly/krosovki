<?php
namespace WBstore\Presenters;

use Config;
use Laracasts\Presenter\Presenter;

class BannerPresenter extends Presenter
{
    use Thumb;
}