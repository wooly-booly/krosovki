<?php
namespace WBstore\Presenters;

use Config;

trait Thumb
{
    public function thumb()
    {
        return $this->getThumbPath($this->image);
    }

    public function thumbs()
    {
        $images = $this->images;

        $thumbs = $images->each(function($item, $key) {
            $item->image = $this->getThumbPath($item->image);
            return $item;
        });

        return $thumbs;
    }

    private function getThumbPath($imagePath)
    {
        $thumbsFolder = Config::get('lfm.thumb_folder_name');
        $pathParts = pathinfo($imagePath);
        
        return $pathParts['dirname'] . '/' . $thumbsFolder . '/' . $pathParts['basename'];
    }
}