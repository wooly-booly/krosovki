<?php
namespace WBstore\Presenters;

use Laracasts\Presenter\Presenter;

class ArticlePresenter extends Presenter
{
    use Thumb;
    
    public function published_date($format = '%d %B %Y')
    {
        if ($this->published_at) {
            return $this->published_at->formatLocalized($format);
        }

        return 'Не опубликовано';
    }
}