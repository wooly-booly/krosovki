<?php

namespace WBstore;

use Illuminate\Database\Eloquent\Model;

class CategoryDescription extends Model
{
    public $timestamps = false;
    protected $table = 'category_description';

    protected $fillable = [
        'title', 'description', 'meta_title',
        'meta_description', 'meta_keyword'
    ];

    public function category()
    {
        return $this->belongsTo('WBstore\Category');
    }
}
