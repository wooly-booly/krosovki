<?php
namespace WBstore;

use Franzose\ClosureTable\Contracts\ClosureTableInterface;

interface CategoryClosureInterface extends ClosureTableInterface
{
}
