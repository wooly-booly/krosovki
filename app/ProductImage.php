<?php

namespace WBstore;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    public $timestamps = false;
    protected $table = 'product_image';

    protected $fillable = [
        'product_id', 'image', 'position'
    ];

    public function product()
    {
        return $this->belongsTo('WBstore\Product');
    }

    public function setPositionAttribute($value)
    {
        $this->attributes['position'] = $value ?: 0;
    }
}
