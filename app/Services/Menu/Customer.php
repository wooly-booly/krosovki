<?php

namespace WBstore\Services\Menu;

class Customer
{
	public function getItems()
	{
		// Initial dummy menu
		return array(
			array('path' => 'account', 'label' => 'John Doe'),
			array('path' => 'logout', 'label' => 'Login | Logout'),
			array('path' => 'register', 'label' => 'Регистрация'),
		);
	}
}