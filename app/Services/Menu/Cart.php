<?php

namespace WBstore\Services\Menu;

class Cart
{
	public function getItems()
	{
		// Initial dummy menu
		return array(
			array('path' => 'cart', 'label' => 'Корзина (3)'),
			array('path' => 'checkout', 'label' => 'Оформить'),
			array('path' => route('store.wishlist'), 'label' => 'Избранное'),
		);
	}
}