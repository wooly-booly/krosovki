<?php

namespace WBstore\Providers;

use Illuminate\Support\ServiceProvider;
use WBstore\View\Composers;

class StoreComposerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app['view']->composer('store.base', Composers\Store\AddCommonSettings::class);
        $this->app['view']->composer('store.common.footer', Composers\Store\AddInfoSection::class);
        $this->app['view']->composer('store.module.category', Composers\Store\AddSidebarCategoriesMenu::class);
        $this->app['view']->composer('store.module.banner.2words', Composers\Store\Add2WordsBanner::class);
        $this->app['view']->composer('store.module.banner.carousel', Composers\Store\AddCarouselBanner::class);
        $this->app['view']->composer('store.module.banner.collection', Composers\Store\AddCollectionBanner::class);
        $this->app['view']->composer('store.module.banner.top5', Composers\Store\AddTop5Banner::class);
    }

    public function register()
    {
        //
    }
}