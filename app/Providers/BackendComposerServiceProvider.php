<?php

namespace WBstore\Providers;

use Illuminate\Support\ServiceProvider;
use WBstore\View\Composers;

class BackendComposerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app['view']->composer('backend.products.form', Composers\Backend\AddCategoriesList::class);
        $this->app['view']->composer('backend.products.form', Composers\Backend\AddBrandsList::class);
    }

    public function register()
    {
        //
    }
}