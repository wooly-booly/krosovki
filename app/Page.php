<?php

namespace WBstore;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'bottom', 'url_key', 'position', 'status'
    ];

    public function description()
    {
        return $this->hasOne('WBstore\PageDescription');
    }
}
