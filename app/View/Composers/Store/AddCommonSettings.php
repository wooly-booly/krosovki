<?php

namespace WBstore\View\Composers\Store;

use Illuminate\View\View;
use WBstore\Setting;

class AddCommonSettings
{
    public function compose(View $view)
    {
        $config = Setting::select('key', 'value')->where('code', '=', Setting::$code)->get();
        $settings = collect();

        foreach ($config as $item) {
            $settings->{Setting::$code}[$item->key] = $item->value;
        }
        
        $view->with('settings', $settings);
    }
}