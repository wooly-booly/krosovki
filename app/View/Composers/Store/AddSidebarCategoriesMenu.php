<?php

namespace WBstore\View\Composers\Store;

use Illuminate\View\View;
use WBstore\Category;

class AddSidebarCategoriesMenu
{
    public function compose(View $view)
    {
        $categoryTree = Category::getTreeByQuery(
            Category::with(['description' => function($q) {
                $q->select('category_id', 'title');
            }])->orderBy('parent_id')->orderBy('position'), 
            ['id', 'url_key', 'status']
        )->toArray();

        $view->with('sidebarCategoryItems', $categoryTree);
    }
}
