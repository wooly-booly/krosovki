<?php

namespace WBstore\View\Composers\Store;

use Illuminate\View\View;
use WBstore\Page;

class AddInfoSection
{
    public function compose(View $view)
    {
        $pages = Page::with(['description' => function ($query) {
                    $query->select('page_id', 'title');
                }])->where('status', '1')->orderBy('position')->get(['id', 'url_key']);
        
        $view->with('pages', $pages);
    }
}