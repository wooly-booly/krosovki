<?php

namespace WBstore\View\Composers\Store;

use Illuminate\View\View;
use WBstore\Setting;
use WBstore\Banner;

class AddCollectionBanner
{
    public function compose(View $view)
    {
        $banner = Banner::get('Collection')->images;
        $setting = Setting::get('collection');

        $view->with('banner', $banner)
            ->with('setting', $setting);
    }
}