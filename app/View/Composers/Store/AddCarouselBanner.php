<?php

namespace WBstore\View\Composers\Store;

use Illuminate\View\View;
use WBstore\Banner;

class AddCarouselBanner
{
    public function compose(View $view)
    {
        $banner = Banner::get('Carousel');
        $carousel = $banner->images;

        $view->with('carousel', $carousel);
    }
}