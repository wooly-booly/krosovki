<?php

namespace WBstore\View\Composers\Store;

use Illuminate\View\View;
use WBstore\Banner;

class AddTop5Banner
{
    public function compose(View $view)
    {
        $banner = Banner::get('Top 5');
        $top5 = $banner->images;

        $view->with('top5', $top5);
    }
}