<?php

namespace WBstore\View\Composers\Store;

use Illuminate\View\View;
use WBstore\Banner;

class Add2WordsBanner
{
    public function compose(View $view)
    {
        $banner = Banner::get('2 words');
        $twoWords = $banner->images->first();

        $view->with('twoWords', $twoWords);
    }
}