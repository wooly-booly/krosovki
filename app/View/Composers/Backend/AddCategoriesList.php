<?php

namespace WBstore\View\Composers\Backend;

use Illuminate\View\View;
use WBstore\Category;

class AddCategoriesList
{
	public function compose(View $view)
	{
		$categories = ["0" => "-- Нет --"];

		foreach(Category::formattedCategories()->get() as $category) {
			$categories[$category->id] = $category->title;
		}

		$view->with('categories', $categories);
	}
}
