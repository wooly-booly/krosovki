<?php

namespace WBstore\View\Composers\Backend;

use Illuminate\View\View;
use WBstore\Brand;

class AddBrandsList
{
    public function compose(View $view)
    {
        $brands = ["0" => "-- Нет --"];

        foreach(Brand::all() as $brand) {
            $brands[$brand->id] = $brand->name;
        }

        $view->with('brands', $brands);
    }
}
