<?php

namespace WBstore;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    public $timestamps = false;
    protected $fillable = ['title', 'image', 'position', 'url_key'];

    public function products()
    {
        return $this->hasMany('WBstore\Product');
    }

    public function setPositionAttribute($value)
    {
        $this->attributes['position'] = $value ?: 0;
    }

    public function setUrlKeyAttribute($value)
    {
        $this->attributes['url_key'] = $value ?: null;
    }
}
