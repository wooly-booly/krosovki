<?php

namespace WBstore\Listeners;

use Unisharp\Laravelfilemanager\Events\ImageIsUploading;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Log;

class LfmImageUploadListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ImageIsUploading  $event
     * @return void
     */
    public function handle(ImageIsUploading $event)
    {
        $articlesPath = public_path() . '/' 
            . config('lfm.images_folder_name') . '/' 
            . config('lfm.article_images_folder_name') . '/';
        $articleThumbWidth = config('lfm.article_thumb_img_width');
        $articleThumbHeight = config('lfm.article_thumb_img_height');

        if (strpos($event->path(), $articlesPath) !== false) {        
            config(['lfm.thumb_img_width' => $articleThumbWidth]); 
            config(['lfm.thumb_img_height' => $articleThumbHeight]);
        }
        // Log::info('Image was uploaded: ' . $event->path());
    }
}
