<?php

namespace WBstore;

use Illuminate\Database\Eloquent\Model;

class ArticleRelated extends Model
{
    public $timestamps = false;
    protected $table = 'article_related';
    protected $fillable = ['article_id', 'related_id'];
}
