<?php

namespace WBstore;

use Illuminate\Database\Eloquent\Model;

class PageDescription extends Model
{
    public $timestamps = false;
    protected $table = 'page_description';

    protected $fillable = [
        'title', 'content', 'meta_description', 'meta_keyword'
    ];

    public function page()
    {
        return $this->belongsTo('WBstore\Page');
    }
}
