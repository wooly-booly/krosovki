<?php

namespace WBstore;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public $timestamps = false;
    public static $code =  'config';

    protected $fillable = [
        'code', 'key', 'value', 'serialized'
    ];

    public static function get($code, $serialized = 0)
    {
        $settings = Setting::where('code', $code)->get();
        $result = [];

        foreach($settings as $setting) {
            $result[$setting['key']] = $setting['value'];
        }

        // ToDo serialized

       return collect($result);
    }
}
