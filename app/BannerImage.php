<?php

namespace WBstore;

use Illuminate\Database\Eloquent\Model;

class BannerImage extends Model
{
    public $timestamps = false;
    protected $table = 'banner_image';
    protected $fillable = ['image', 'alt', 'link', 'position'];
    
    public function banner()
    {
        return $this->belongsTo('WBstore\Banner');
    }
}
