<?php

namespace WBstore;

use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;
use WBstore\Presenters\ArticlePresenter;

class Article extends Model
{
    use PresentableTrait;

    public $timestamps = false;
    protected $presenter = ArticlePresenter::class;
    protected $dates = ['published_at'];
    protected $fillable = [
        'author_id', 'url_key', 'image', 'viewed', 'published_at'
    ];

    public function description()
    {
        return $this->hasOne('WBstore\ArticleDescription');
    }

    public function relatedArticles()
    {
        return $this->belongsToMany('WBstore\Article', 'article_related', 'article_id', 'related_id');
    }

    public function relatedProducts()
    {
        return $this->belongsToMany('WBstore\Product');
    }

    public function setPublishedAtAttribute($value)
    {
        $this->attributes['published_at'] = $value ?: null;
    }
}
