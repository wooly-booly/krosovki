<?php

namespace WBstore;

use Illuminate\Database\Eloquent\Model;
use WBstore\Presenters\ProductPresenter;
use Laracasts\Presenter\PresentableTrait;

class Product extends Model
{
    use PresentableTrait;

    protected $presenter = ProductPresenter::class;
    protected $fillable = [
        'brand_id', 'url_key', 'image', 'price', 'position', 'status'
    ];
    
    public function categories()
    {
        return $this->belongsToMany('WBstore\Category', 'product_category');
	}

    public function description()
    {
        return $this->hasOne('WBstore\ProductDescription');
    }

    public function images()
    {
        return $this->hasMany('WBstore\ProductImage');
    }

    public function brand()
    {
        return $this->belongsTo('WBstore\Brand');
    }

    public function setPositionAttribute($value)
    {
        $this->attributes['position'] = $value ?: 0;
    }

    public function setUrlKeyAttribute($value)
    {
        $this->attributes['url_key'] = $value ?: null;
    }
}
