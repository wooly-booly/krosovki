<?php

namespace WBstore;

use Illuminate\Database\Eloquent\Model;

class ArticleProductRelated extends Model
{
    public $timestamps = false;
    protected $table = 'article_product';
    protected $fillable = ['article_id', 'product_id'];

    public function products()
    {
        return $this->belongsToMany('WBstore\Product', 'product_category');
    }
}
