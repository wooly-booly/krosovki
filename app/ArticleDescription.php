<?php

namespace WBstore;

use Illuminate\Database\Eloquent\Model;

class ArticleDescription extends Model
{
    public $timestamps = false;
    protected $table = 'article_description';

    protected $fillable = [
        'title', 'body', 'excerpt', 'meta_description', 'meta_keyword'
    ];

    public function article()
    {
        return $this->belongsTo('WBstore\Article');
    }
}
