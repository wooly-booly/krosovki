<?php

namespace WBstore;

use Illuminate\Database\Eloquent\Model;
use WBstore\Presenters\BannerPresenter;
use Laracasts\Presenter\PresentableTrait;

class Banner extends Model
{
    use PresentableTrait;

    protected $presenter = BannerPresenter::class;
    public $timestamps = false;
    public $fillable = ['title', 'status'];

    public function images()
    {
        return $this->hasMany('WBstore\BannerImage');
    }

    public static function get($title)
    {
        return self::where('title', $title)->first();
    }
}
