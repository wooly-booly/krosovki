const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

// elixir((mix) => {
//     mix.sass('app.scss')
//        .webpack('app.js');
// });



elixir(function(mix) {
    mix.styles([
            'bootstrap.min.css',
            'animate.css',
            'materialdesignicons.min.css',
            'jquery.simpleLens.css',
            'jquery-ui.min.css',
            'jquery.jgrowl.min.css',
            'meanmenu.min.css',
            'nivo-slider.css',
            'owl.carousel.css',
            'style.css',
            'responsive.css'
        ], 
        'public/css/site.css'
    ).copy('resources/assets/fonts', 'public/fonts')
    .copy('resources/assets/js', 'public/js');
});
