@extends('backend.base')

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('breadcrumbs', Breadcrumbs::render('articles.index'))
@section('page-title', 'Статьи')

@push('css')
    <link href="/_admin/assets/plugins/jquery-datatables/datatables.min.css" rel="stylesheet" type="text/css">
    <link href="/_admin/assets/plugins/jquery-datatable/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
@endpush

@push('js')
    <script src="/_admin/assets/plugins/jquery-datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/_admin/assets/js/datatable.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready( function () {
            // articles DataTable
            $('#articles .table').dataTable({
                "dom": "<'row'<'col-md-6'l <'toolbar'>><'col-md-6'f>r>t<'row'<'col-md-12'p i>>",
                "language": <?php include('_admin/assets/plugins/jquery-datatables/datatables.lang.Russian.json'); ?>,
                "order": [[ 2, "asc" ]],
                "processing": true,
                "serverSide": true,
                "ajax": '{!! route('articles.datatable') !!}',
                "columns": [{
                        "data": "id",
                        "sortable": false,
                        "searchable": false,
                        "render": function (id, type, full)  {
                            return '<div class="checkbox check-default"><input value="'+id+'" id="checkbox'+id+'" type="checkbox"><label for="checkbox'+id+'"></label></div>';
                        }
                    },{
                        "data": "description.title"
                    },{
                        "data": "published_at",
                        "searchable": false,
                        "class": "text-center"
                    },{
                        "data": "id",
                        "searchable": false,
                        "sortable": false,
                        "class": "text-center",
                        "render": function (id, type, full)  {
                            var linkDelete = '{!! route("articles.destroy", ["articles" => '#id_delete']) !!}';
                            var linkEdit = '{!! route("articles.edit", ["articles" => '#id_edit']) !!}';
                            linkDelete = linkDelete.replace("#id_delete", id);
                            linkEdit = linkEdit.replace("#id_edit", id);
                             
                            return '<a href="'+ linkEdit +'" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a><a href="'+ linkDelete +'" data-toggle="tooltip" data-method="delete" class="postback btn btn-danger" data-original-title="Удалить"><i class="fa fa-trash-o"></i></a>';
                        }
                    }
                ]
            });

            $("div.toolbar").html('<div class="table-tools-actions"><a class="btn btn-primary" href="{{ route("articles.create") }}" style="margin-left:12px">Добавить</a></div>');

            $('#articles .dataTables_filter input').addClass("input-medium ");
            $('#articles .dataTables_length select').removeClass().addClass("select2-wrapper span12");
            $(".select2-wrapper").select2({minimumResultsForSearch: -1});

            // partials.destroy_record
            setDelEvent('a.postback');
        });
    </script>
@endpush

@section('content')
       <div class="row-fluid">
        <div class="span12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4>Список страниц</h4>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="grid-body ">
                    <div id="articles">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width: 2%"></th>
                                            <th style="width: 50%">Название</th>
                                            <th style="width: 20%">Опубликовано</th>
                                            <th style="width: 10%" class="text-center">Действие</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    @include('backend.partials.destroy_record')
                </div>
            </div>
        </div>
    </div>
@stop