@extends('backend.base')

@section('breadcrumbs', Breadcrumbs::render('articles.edit', $article))
@section('page-title', 'Редактировать статью')

@section('content')
    <!-- BEGIN PlACE PAGE CONTENT HERE -->
    <div class="row">
        <div class="col-md-12">
            @include('backend.articles.form')
        </div>
    </div>
    <!-- END PLACE PAGE CONTENT HERE -->
@stop