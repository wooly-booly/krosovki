@push('css')
    {{-- HTML5 WYSIYG --}}
    <link href="/_admin/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css">
    <link href="/_admin/assets/plugins/bootstrap-wysihtml5/wysiwyg-color.css" rel="stylesheet" type="text/css">
    {{-- DateTime Picker --}}
    <link href="/_admin/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">
@endpush

@push('js')
    {{-- HTML5 WYSIYG --}}
    <script src="/_admin/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
    <script src="/_admin/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
    {{-- DateTime Picker --}}
    <script src="/_admin/assets/plugins/moment/js/moment.min.js" type="text/javascript"></script>
    <script src="/_admin/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    {{-- Select with Autocomplete --}}
    <script src="/_admin/assets/js/select-autocomplete.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#text-editor').wysihtml5({
                stylesheets: ['/_admin/assets/plugins/bootstrap-wysihtml5/wysiwyg-color.css']
            });
            $('#text-editor-excerpt').wysihtml5({
                stylesheets: ['/_admin/assets/plugins/bootstrap-wysihtml5/wysiwyg-color.css']
            });

            // Тabs
            $('#articleTab a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            // Set Filemanager
            filemanager.set('lfm');
            filemanager.setImageDelButton('#delImg');

            // Published at
            $('input[name=published_at]').datetimepicker({
                allowInputToggle: true,
                format: 'YYYY-MM-DD HH:mm:ss',
                showClear: true,
                defaultDate: '{{ old('published_at', $article->published_at) }}'
            });

            // Select Related Articles
            var articles = $("#articles");
            autocomplete(articles, '{!! route('articles.autocomplete') !!}');

            // Select Related Products
            var products = $("#products");
            autocomplete(products, '{!! route('products.autocomplete') !!}');
        }); 
    </script>
@endpush

{!! Form::model($article, [
    'method' => $article->exists ? 'put' : 'post',
    'route' => $article->exists ? ['articles.update', $article->id] : ['articles.store']
]) !!}

<ul class="nav nav-tabs" id="articleTab">
    <li class="active"><a href="#tab-general">Основное</a></li>
    <li><a href="#tab-data">Данные</a></li>
    <li><a href="#tab-related">Связи</a></li>
</ul>

<div class="tab-content">
    {{-- Tab General --}}
    <div class="tab-pane active" id="tab-general">
        <div class="form-group">
            <label class="form-label">Название</label>
            <div class="controls">
                {!! Form::text('description[title]', null, ['class' => 'form-control' . (!$errors->has('description.title') ? '' : ' error') ]) !!}
                @if ($errors->has('description.title'))
                    <label class="error" for="description[title]">{{ $errors->first('description.title') }}</label>
                @endif
            </div>
        </div>

        <div class="form-group {{ !$errors->has('description.excerpt') ?: 'has-error'  }}">
            <label class="form-label">Краткое описание</label>
            <div class="controls">
                {!! Form::textarea('description[excerpt]', null, ['class' => 'form-control', 'id' => 'text-editor-excerpt', 'rows' => '4']) !!}
            </div>
            @if ($errors->has('description.excerpt'))
                <label class="error" for="description[excerpt]">{{ $errors->first('description.excerpt') }}</label>
            @endif
        </div>

        <div class="form-group {{ !$errors->has('description.body') ?: 'has-error'  }}">
            <label class="form-label">Статья</label>
            <div class="controls">
                {!! Form::textarea('description[body]', null, ['class' => 'form-control', 'id' => 'text-editor', 'rows' => '10']) !!}
            </div>
            @if ($errors->has('description.body'))
                <label class="error" for="description[body]">{{ $errors->first('description.body') }}</label>
            @endif
        </div>
    </div>

    {{-- Tab Data --}}
    <div class="tab-pane" id="tab-data">
        <div class="form-group row">
            <div class="col-md-12">
                {!! Form::label('published_at') !!}
            </div>
            <div class="col-md-4">
                {!! Form::text('published_at', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="form-label">Url-ссылка</label>
            <span class="help">пример: "то-что-будет-в-адресной-строке"</span>
            <div class="controls">
                {!! Form::text('url_key', null, ['class' => 'form-control' . (!$errors->has('url_key') ? '' : ' error') ]) !!}
                @if ($errors->has('url_key'))
                    <label class="error" for="url_key">{{ $errors->first('url_key') }}</label>
                @endif
            </div>
        </div>

        <div class="form-group {{ !$errors->has('description.meta_description') ?: 'has-error'  }}">
            <label class="form-label">Метатег Описание</label>
            <div class="controls">
                {!! Form::textarea('description[meta_description]', null, ['class' => 'form-control', 'rows' => '2']) !!}
            </div>
            @if ($errors->has('description.meta_description'))
                <label class="error" for="description[meta_description]">{{ $errors->first('description.meta_description') }}</label>
            @endif
        </div>

        <div class="form-group {{ !$errors->has('description.meta_keyword') ?: 'has-error'  }}">
            <label class="form-label">Метатег Ключевые слова</label>
            <div class="controls">
                {!! Form::textarea('description[meta_keyword]', null, ['class' => 'form-control', 'rows' => '2']) !!}
            </div>
            @if ($errors->has('description.meta_keyword'))
                <label class="error" for="description[meta_keyword]">{{ $errors->first('description.meta_keyword') }}</label>
            @endif
        </div>
        
        <div class="form-group">
            <label class="form-label">Изображение</label>
            <div class="controls">
                <div class="input-group">
                    <span class="input-group-btn">
                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                            <i class="fa fa-picture-o"></i> Выбрать
                        </a>
                    </span>
                    <input id="thumbnail" class="form-control" type="text" name="image"
                    readonly value="{{ $article->exists ? $article->image : '' }}">
                </div>
                <div class='close-image'>
                    <button type='button' id="delImg" class='close'>×</button>
                    <img id="holder" src="{{ $article->exists ? $article->present()->thumb() : '' }}">
                </div>
            </div>
            @if ($errors->has('image'))
                <label class="error" for="image">{{ $errors->first('image') }}</label>
            @endif 
        </div>
    </div>

    <div class="tab-pane" id="tab-related">
        <div class="form-group">
            <label class="form-label">Связанные статьи</label>
            <span class="help">автозаполнение</span>
            <div class="controls">
                <select id="articles" name="related_articles[]" multiple="multiple" style="width: 100%">
                    @if ($article->exists && $article->relatedArticlesById)
                        @foreach ($article->relatedArticlesById as $id => $item)
                            <option selected="selected" value="{{ $id }}">
                                {{ $item }}
                            </option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="form-label">Связанные товары</label>
            <span class="help">автозаполнение</span>
            <div class="controls">
                <select id="products" name="related_products[]" multiple="multiple" style="width: 100%">
                    @if ($article->exists && $article->relatedProductsById)
                        @foreach ($article->relatedProductsById as $id => $item)
                            <option selected="selected" value="{{ $id }}">
                                {{ $item }}
                            </option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>

</div>

{!! Form::submit($article->exists ? 'Обновить' : 'Сохранить', ['class' => 'btn btn-primary']) !!}

{!! Form::close() !!}

@include('backend.partials.filemanager')