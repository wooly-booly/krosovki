@extends('backend.base')

@section('breadcrumbs', Breadcrumbs::render('categories.create'))
@section('page-title', 'Добавить категорию')

@section('content')

<!-- BEGIN PlACE PAGE CONTENT HERE -->
<div class="row">
    <div class="col-md-12">
        <div class="grid simple">
            <div class="grid-title no-border">
                <h4>Добавить категорию</h4>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                </div>
            </div>

            <div class="grid-body no-border" style="display: block;">
                <div class="col-md-8 col-sm-8 col-xs-8">

                    {{-- CREATE FORM START  --}}
                    @include('backend.categories.form')       
                    {{-- CREATE FORM END --}}
                    
                </div>
            </div>

        </div>
    </div>
</div>
<!-- END PLACE PAGE CONTENT HERE -->

@stop