@push('css')
	{{-- HTML5 WYSIYG  --}}
	<link href="/_admin/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css">

@endpush

@push('js')
	{{-- HTML5 WYSIYG  --}}
	<script src="/_admin/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
	<script src="/_admin/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>

    {{-- Select Autocomplete --}}
	<script src="/_admin/assets/js/select-autocomplete.js" type="text/javascript"></script>

	<script type="text/javascript">
		$(document).ready( function () {
			$('#text-editor').wysihtml5();
			$("#status").select2({minimumResultsForSearch: Infinity});

			// Select Parent Category Autocomplete
			var ancestor = $("#ancestor");

			autocomplete(ancestor, '{!! route('categories.autocomplete') !!}');

			@if ($category->parent != null)
				ancestor.append('<option value="{{ $category->parent_id }}" selected="selected">{{ $category->parent->description->title }}</option>');
				ancestor.trigger('change');
			@endif

			// Set Filemanager
			filemanager.set('lfm');
			filemanager.setImageDelButton('#delImg');
		});
	</script>
@endpush

{!! Form::model($category, [
	'method' => $category->exists ? 'put' : 'post',
	'route' => $category->exists ? ['categories.update', $category->id] : ['categories.store'],
	'enctype' => 'multipart/form-data',
]) !!}

	<div class="form-group">
		<label class="form-label">Название</label>
		<div class="controls">
			{!! Form::text('description[title]', null, ['class' => 'form-control' . (!$errors->has('description.title') ? '' : ' error') ]) !!}
			@if ($errors->has('description.title'))
				<label class="error" for="description[title]">{{ $errors->first('description.title') }}</label>
			@endif
		</div>
	</div>

	<div class="form-group">
		<label class="form-label">Url-ссылка</label>
		<span class="help">пример: "то-что-будет-в-адресной-строке"</span>
		<div class="controls">
			{!! Form::text('url_key', null, ['class' => 'form-control' . (!$errors->has('url_key') ? '' : ' error') ]) !!}
			@if ($errors->has('url_key'))
				<label class="error" for="url_key">{{ $errors->first('url_key') }}</label>
			@endif
		</div>
	</div>

	<div class="form-group {{ !$errors->has('description.description') ?: 'has-error'  }}">
		<label class="form-label">Описание</label>
		<div class="controls">
			{!! Form::textarea('description[description]', null, ['class' => 'form-control', 'id' => 'text-editor', 'rows' => '7']) !!}
		</div>
		@if ($errors->has('description.description'))
			<label class="error" for="description[description]">{{ $errors->first('description.description') }}</label>
		@endif
	</div>

	<div class="form-group">
		<label class="form-label">Позиция</label>
		<span class="help">для сортировки</span>
		<div class="controls">
			{!! Form::text('position', null, ['class' => 'form-control' . (!$errors->has('position') ? '' : ' error') ]) !!}
			@if ($errors->has('position'))
				<label class="error" for="position">{{ $errors->first('position') }}</label>
			@endif
		</div>
	</div>

	<div class="form-group">
		<label class="form-label">Родитель</label>
		<span class="help">автозаполнение</span>
		<div class="controls">
			<select id="ancestor" name="parent_id" style="width: 100%">
			</select>
		</div>
	</div>

	<div class="form-group {{ !$errors->has('description.meta_description') ?: 'has-error'  }}">
		<label class="form-label">Метатег Описание</label>
		<div class="controls">
			{!! Form::textarea('description[meta_description]', null, ['class' => 'form-control', 'rows' => '2']) !!}
		</div>
		@if ($errors->has('description.meta_description'))
			<label class="error" for="description[meta_description]">{{ $errors->first('description.meta_description') }}</label>
		@endif
	</div>

	<div class="form-group {{ !$errors->has('description.meta_keyword') ?: 'has-error'  }}">
		<label class="form-label">Метатег Ключевые слова</label>
		<div class="controls">
			{!! Form::textarea('description[meta_keyword]', null, ['class' => 'form-control', 'rows' => '2']) !!}
		</div>
		@if ($errors->has('description.meta_keyword'))
			<label class="error" for="description[meta_keyword]">{{ $errors->first('description.meta_keyword') }}</label>
		@endif
	</div>

	<div class="form-group">
		<label class="form-label">Изображение</label>
		<div class="controls">
			<div class="input-group">
				<span class="input-group-btn">
					<a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
						<i class="fa fa-picture-o"></i> Выбрать
					</a>
				</span>
				<input id="thumbnail" class="form-control" type="text" name="image"
				readonly value="{{ $category->exists ? $category->image : '' }}">
			</div>
			<div class='close-image'>
				<button type='button' id="delImg" class='close'>×</button>
				<img id="holder" src="{{ $category->exists ? $category->image : '' }}">
			</div>
		</div>
		@if ($errors->has('image'))
			<label class="error" for="image">{{ $errors->first('image') }}</label>
		@endif
	</div>

	<div class="form-group">
		<label class="form-label">Статус</label>
		<div class="controls">
		    {!! Form::select('status', ['1'=>'Включено', '0'=>'Отключено'], $category->status, ['id' => 'status', 'style' => 'width: 100%']) !!}
		</div>
	</div>

	{!! Form::submit($category->exists ? 'Обновить' : 'Сохранить', ['class' => 'btn btn-success btn-cons']) !!}

{!! Form::close() !!}

@include('backend.partials.filemanager')