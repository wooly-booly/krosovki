@push('js')
    <script type="text/javascript">
        $(document).ready(function(){

            // Product Form Tabs
            $('#SettingsFormTab a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            // Set Filemanager
            filemanager.set('lfm-logo');
            filemanager.setImageDelButton('#delLogo');

            filemanager.set('lfm-icon');
            filemanager.setImageDelButton('#delIcon');
        });
    </script>
@endpush

{!! Form::model($settings, [
    'method' => 'put',
    'route' => ['settings.update'],
    'enctype' => 'multipart/form-data'
]) !!}

<ul class="nav nav-tabs" id="SettingsFormTab">
    <li class="active"><a href="#tab-general">Основное</a></li>
    <li><a href="#tab-images">Изображения</a></li>
</ul>
 
<div class="tab-content">
    {{-- Tab General --}}
    <div class="tab-pane active" id="tab-general">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-label">Название магазина</label>
                    <div class="controls">
                        {!! Form::text('config[store_title]', null, ['class' => 'form-control' . (!$errors->has('config.store_title') ? '' : ' error') ]) !!}
                        @if ($errors->has('config.store_title'))
                            <label class="error" for="config[store_title]">{{ $errors->first('config.store_title') }}</label>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="form-label">Адрес</label>
                    <div class="controls">
                        {!! Form::text('config[address]', null, ['class' => 'form-control' . (!$errors->has('config.address') ? '' : ' error') ]) !!}
                        @if ($errors->has('config.address'))
                            <label class="error" for="config[address]">{{ $errors->first('config.address') }}</label>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="form-label">Email</label>
                    <div class="controls">
                        {!! Form::text('config[email]', null, ['class' => 'form-control' . (!$errors->has('config.email') ? '' : ' error') ]) !!}
                        @if ($errors->has('config.email'))
                            <label class="error" for="config[email]">{{ $errors->first('config.email') }}</label>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ !$errors->has('config.comment') ?: 'has-error'  }}">
                    <label class="form-label">Комментарий</label>
                    <div class="controls">
                        {!! Form::textarea('config[comment]', null, ['class' => 'form-control', 'rows' => '2']) !!}
                    </div>
                    @if ($errors->has('config.comment'))
                        <label class="error" for="config[comment]">{{ $errors->first('config.comment') }}</label>
                    @endif
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-label">Время работы</label>
                    <div class="controls">
                        {!! Form::text('config[open_hours]', null, ['class' => 'form-control' . (!$errors->has('config.open_hours') ? '' : ' error') ]) !!}
                        @if ($errors->has('config.open_hours'))
                            <label class="error" for="config[open_hours]">{{ $errors->first('config.open_hours') }}</label>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="form-label">Телефон</label>
                    <div class="controls">
                        {!! Form::text('config[phone]', null, ['class' => 'form-control' . (!$errors->has('config.phone') ? '' : ' error') ]) !!}
                        @if ($errors->has('config.phone'))
                            <label class="error" for="config[phone]">{{ $errors->first('config.phone') }}</label>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="form-label">Метатег Заголовок</label>
                    <div class="controls">
                        {!! Form::text('config[meta_title]', null, ['class' => 'form-control' . (!$errors->has('config.meta_title') ? '' : ' error') ]) !!}
                        @if ($errors->has('config.meta_title'))
                            <label class="error" for="config[meta_title]">{{ $errors->first('config.meta_title') }}</label>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ !$errors->has('meta_description') ?: 'has-error'  }}">
                    <label class="form-label">Метатег Описание</label>
                    <div class="controls">
                        {!! Form::textarea('config[meta_description]', null, ['class' => 'form-control', 'rows' => '2']) !!}
                    </div>
                    @if ($errors->has('config.meta_description'))
                        <label class="error" for="config[meta_description]">{{ $errors->first('config.meta_description') }}</label>
                    @endif
                </div>

                <div class="form-group {{ !$errors->has('meta_keyword') ?: 'has-error'  }}">
                    <label class="form-label">Метатег Ключевые слова</label>
                    <div class="controls">
                        {!! Form::textarea('config[meta_keyword]', null, ['class' => 'form-control', 'rows' => '2']) !!}
                    </div>
                    @if ($errors->has('config.meta_keyword'))
                        <label class="error" for="config[meta_keyword]">{{ $errors->first('config.meta_keyword') }}</label>
                    @endif
                </div>    
            </div>
        </div>
    </div>

    {{-- Tab Images --}}
    <div class="tab-pane" id="tab-images">
        <div class="form-group">
            <label class="form-label">Логотип</label>
            <div class="controls">
                <div class="input-group">
                    <span class="input-group-btn">
                        <a id="lfm-logo" data-input="thumbnail-logo" data-preview="holder-logo" class="btn btn-primary">
                            <i class="fa fa-picture-o"></i> Выбрать
                        </a>
                    </span>
                    <input id="thumbnail-logo" class="form-control" type="text" name="config[logo]"
                    readonly value="{{ $settings->config['logo'] ?? '' }}">
                </div>
                <div class='close-image'>
                    <button type='button' id='delLogo' class='close'>×</button>
                    <img id="holder-logo" src="{{ $settings->config['logo'] ?? '' }}">
                </div>
            </div>
            @if ($errors->has('config.logo'))
                <label class="error" for="config[logo]">{{ $errors->first('config.logo') }}</label>
            @endif 
        </div>

        <div class="form-group">
            <label class="form-label">Иконка</label>
            <div class="controls">
                <div class="input-group">
                    <span class="input-group-btn">
                        <a id="lfm-icon" data-input="thumbnail-icon" data-preview="holder-icon" class="btn btn-primary">
                            <i class="fa fa-picture-o"></i> Выбрать
                        </a>
                    </span>
                    <input id="thumbnail-icon" class="form-control" type="text" name="config[icon]"
                    readonly value="{{ $settings->config['icon'] ?? '' }}">
                </div>
                <div class='close-image'>
                    <button type='button' id="delIcon" class='close'>×</button>
                    <img id="holder-icon" src="{{ $settings->config['icon'] ?? '' }}">
                </div>
            </div>
            @if ($errors->has('config.icon'))
                <label class="error" for="config[icon]">{{ $errors->first('config.icon') }}</label>
            @endif 
        </div>
    </div>
</div>

<button type="submit" class="btn btn-success btn-cons">Сохранить</button>
{!! Form::close() !!}

@include('backend.partials.filemanager')
