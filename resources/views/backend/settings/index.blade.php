@extends('backend.base')

@section('breadcrumbs', Breadcrumbs::render('settings.index'))
@section('page-title', 'Настройки')

@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('backend.settings.form')
        </div>
    </div>
@stop