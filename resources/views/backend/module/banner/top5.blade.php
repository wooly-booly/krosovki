@extends('backend.base')

@push('css')
    <style>
        #images td img { margin-top:15px; max-height:100px; }
        #images td {vertical-align: middle;}
    </style>
@endpush

@push('js')
    <script type="text/javascript">
        var image_row = 1;
        function addImage(img) {
            img = img || {image:'', alt:'', link:'', position:''};

            html = '<tr id="image-row' + image_row + '">';
            // Choose image
            html += '<td><div class="controls"><div class="input-group"><span class="input-group-btn"><a id="lfm' + image_row + '" data-input="thumbnail' + image_row + '" data-preview="holder' + image_row + '" class="btn btn-primary"><i class="fa fa-picture-o"></i> Выбрать</a></span><input id="thumbnail' + image_row + '" class="form-control" type="text" name="banner[' + image_row + '][image]" readonly value="' + img.image + '"></div></div></td>';
            // Title
            html += '<td class="text-right"><input type="text" name="banner[' + image_row + '][alt]" placeholder="Заголовок" class="form-control" value="' + img.alt + '" /></td>';
            // Link
            html += '<td class="text-right"><input type="text" name="banner[' + image_row + '][link]" placeholder="Ссылка" class="form-control" value="' + img.link + '" /></td>';
            // Position
            html += '<td class="text-right"><input type="text" name="banner[' + image_row + '][position]" placeholder="Позиция" class="form-control" value="' + img.position + '" /></td>';
            // Thumbnail
            html += '<td><div class="close-image"><button type="button" onclick="$(\'#image-row' + image_row  + '\').remove();" class="close">×</button><img id="holder' + image_row + '" src="' + img.image + '"></div></td>';
            html += '</tr>';

            $('#images tbody').append(html);

            filemanager.set('lfm' + image_row);

            image_row++;
        }

        $(document).ready(function(){
            //Images
            var imgs = {!! !empty($banner->images) ? $banner->images->toJson() : 'null' !!};

            if (imgs) {
                for (var i=0; i < imgs.length; i++) {
                    addImage(imgs[i]);
                }
            }
        });
    </script>
@endpush

@section('breadcrumbs', Breadcrumbs::render('banner.top5'))
@section('page-title', 'Top 5')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-title no-border">
                    <h4>Редактировать</h4>
                    <div class="tools">
                        <a class="collapse"></a>
                    </div>
                </div>
                <div class="grid-body no-border" style="display: block;">       
                    <div class="table-responsive">
                        <form method="post">
                            {{ csrf_field() }}
                            <table id="images" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td style="width: 37%">Изображение</td>
                                        <td style="width: 21%">Заголовок</td>
                                        <td style="width: 14%">Ссылка</td>
                                        <td style="width: 14%">Позиция</td>
                                        <td style="width: 14%"></td>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="4"></td>

                                        <td class="text-left">
                                            <button type="button" onclick="addImage();" data-toggle="tooltip" title="" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                            <button type="submit" class="btn btn-success btn-cons">Сохранить</button>
                        </form>
                        @include('backend.partials.filemanager')
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop