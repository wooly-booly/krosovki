@extends('backend.base')

@push('css')
    <style>
        #images td img { margin-top:15px; max-height:100px; }
        #images td {vertical-align: middle;}
    </style>
@endpush

@push('js')
    <script type="text/javascript">
        $(document).ready(function(){
            // Filemanager
            filemanager.set('lfm0');
            filemanager.setImageDelButton('#delImg0');

            filemanager.set('lfm1');
            filemanager.setImageDelButton('#delImg1');

            filemanager.set('lfm2');
            filemanager.setImageDelButton('#delImg2');
        });
    </script>
@endpush

@section('breadcrumbs', Breadcrumbs::render('banner.collection'))
@section('page-title', 'Collection')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="grid simple">
            <div class="grid-title no-border">
                <h4>Редактировать</h4>
                <div class="tools">
                    <a class="collapse"></a>
                </div>
            </div>
            <div class="grid-body no-border">
                {!! Form::model($banner, ['method' => 'post']) !!}
                <div class="row">
                    <div class="col-md-6">
                        <h3>Надпись <span class="semi-bold">слева</span></h3>
                        <div class="form-group">
                            <label class="form-label">Загаловок</label>
                            <div class="controls">
                                {!! Form::text('setting[title_left]', $setting['title_left'] ?? '', ['class' => 'form-control' . (!$errors->has('setting.title_left') ? '' : ' error') ]) !!}
                                {{-- @if ($errors->has('setting.title_left'))
                                    <label class="error" for="setting[title_left]">{{ $errors->first('setting.title_left') }}</label>
                                @endif --}}
                            </div>
                        </div>

                        <div class="form-group {{ !$errors->has('setting.description_left') ?: 'has-error'  }}">
                            <label class="form-label">Описание</label>
                            <span>в качестве разделителя строк можно использовать тег: &lt;br&gt;</span>
                            <div class="controls">
                                {!! Form::textarea('setting[description_left]', $setting['description_left'] ?? '', ['class' => 'form-control', 'rows' => '2']) !!}
                            </div>
                            {{-- @if ($errors->has('setting.description_left'))
                                <label class="error" for="setting[description_left]">{{ $errors->first('setting.description_left') }}</label>
                            @endif --}}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <h3>Надпись <span class="semi-bold">справа</span></h3>
                        <div class="form-group">
                            <label class="form-label">Загаловок</label>
                            <div class="controls">
                                {!! Form::text('setting[title_right]', $setting['title_right'] ?? '', ['class' => 'form-control' . (!$errors->has('setting.title_right') ? '' : ' error') ]) !!}
                                {{-- @if ($errors->has('setting.title_right'))
                                    <label class="error" for="setting[title_right]">{{ $errors->first('setting.title_right') }}</label>
                                @endif --}}
                            </div>
                        </div>

                        <div class="form-group {{ !$errors->has('setting.description_right') ?: 'has-error'  }}">
                            <label class="form-label">Описание</label>
                            <div class="controls">
                                {!! Form::textarea('setting[description_right]', $setting['description_right'] ?? '', ['class' => 'form-control', 'rows' => '2']) !!}
                            </div>
                            {{-- @if ($errors->has('setting.description_right'))
                                <label class="error" for="setting[description_right]">{{ $errors->first('setting.description_right') }}</label>
                            @endif --}}
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table id="images" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td style="width: 7%"><b>Расположение</b></td>
                                    <td style="width: 37%"><b>Изображение</b></td>
                                    <td style="width: 21%"><b>Заголовок</b></td>
                                    <td style="width: 21%"><b>Ссылка</b></td>
                                    <td style="width: 14%"></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Слева</td>
                                    <td>
                                        <div class="controls">
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <a id="lfm0" data-input="thumbnail0" data-preview="holder0" class="btn btn-primary"><i class="fa fa-picture-o"></i> Выбрать
                                                    </a>
                                                </span>
                                                {!! Form::text('banner[0][image]', $banner[0]['image'] ?? '', ['id' => 'thumbnail0', 'readonly' => 'readonly', 'class' => 'form-control' . (!$errors->has('banner.0.image') ? '' : ' error') ]) !!}
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        {!! Form::text('banner[0][alt]', $banner[0]['alt'] ?? '', ['placeholder' => 'Заголовок', 'class' => 'form-control' . (!$errors->has('banner.0.alt') ? '' : ' error') ]) !!}
                                    </td>
                                    <td>
                                        {!! Form::text('banner[0][link]', $banner[0]['link'] ?? '', ['placeholder' => 'Ссылка', 'class' => 'form-control' . (!$errors->has('banner.0.link') ? '' : ' error') ]) !!}
                                    </td>
                                    <td>
                                        <div class="close-image">
                                            <button type="button" id="delImg0" class="close">×</button>
                                            <img id="holder0" src="{{ $banner[0]['image'] ?? '' }}">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Справа</td>
                                    <td>
                                        <div class="controls">
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <a id="lfm1" data-input="thumbnail1" data-preview="holder1" class="btn btn-primary"><i class="fa fa-picture-o"></i> Выбрать
                                                    </a>
                                                </span>
                                                {!! Form::text('banner[1][image]', $banner[1]['image'] ?? '', ['id' => 'thumbnail1', 'readonly' => 'readonly', 'class' => 'form-control' . (!$errors->has('banner.1.image') ? '' : ' error') ]) !!}
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        {!! Form::text('banner[1][alt]', $banner[1]['alt'] ?? '', ['placeholder' => 'Заголовок', 'class' => 'form-control' . (!$errors->has('banner.1.alt') ? '' : ' error') ]) !!}
                                    </td>
                                    <td>
                                        {!! Form::text('banner[1][link]', $banner[1]['link'] ?? '', ['placeholder' => 'Ссылка', 'class' => 'form-control' . (!$errors->has('banner.1.link') ? '' : ' error') ]) !!}
                                    </td>
                                    <td>
                                        <div class="close-image">
                                            <button type="button" id="delImg1" class="close">×</button>
                                            <img id="holder1" src="{{ $banner[1]['image'] ?? '' }}">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Снизу</td>
                                    <td>
                                        <div class="controls">
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <a id="lfm2" data-input="thumbnail2" data-preview="holder2" class="btn btn-primary"><i class="fa fa-picture-o"></i> Выбрать
                                                    </a>
                                                </span>
                                                {!! Form::text('banner[2][image]', $banner[2]['image'] ?? '', ['id' => 'thumbnail2', 'readonly' => 'readonly', 'class' => 'form-control' . (!$errors->has('banner.2.image') ? '' : ' error') ]) !!}
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        {!! Form::text('banner[2][alt]', $banner[2]['alt'] ?? '', ['placeholder' => 'Заголовок', 'class' => 'form-control' . (!$errors->has('banner.2.alt') ? '' : ' error') ]) !!}
                                    </td>
                                    <td>
                                        {!! Form::text('banner[2][link]', $banner[2]['link'] ?? '', ['placeholder' => 'Ссылка', 'class' => 'form-control' . (!$errors->has('banner.2.link') ? '' : ' error') ]) !!}
                                    </td>
                                    <td>
                                        <div class="close-image">
                                            <button type="button" id="delImg2" class="close">×</button>
                                            <img id="holder2" src="{{ $banner[2]['image'] ?? '' }}">
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <button type="submit" class="btn btn-success btn-cons">Сохранить</button>

                <input type="hidden" name="banner[0][position]" value="0">
                <input type="hidden" name="banner[1][position]" value="1">
                <input type="hidden" name="banner[2][position]" value="2">

                @include('backend.partials.filemanager')
                {!! Form::close() !!}
            </div>

        </div>
    </div>
</div>
@stop