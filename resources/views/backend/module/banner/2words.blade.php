@extends('backend.base')

@push('js')
    <script type="text/javascript">
        $(document).ready(function(){
            // Filemanager
            filemanager.set('lfm');
            filemanager.setImageDelButton('#delImg');
        });
    </script>
@endpush

@section('breadcrumbs', Breadcrumbs::render('banner.2words'))
@section('page-title', '2 Words')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="grid simple">
                <div class="grid-title no-border">
                    <h4>Редактировать</h4>
                    <div class="tools">
                        <a class="collapse"></a>
                    </div>
                </div>
                <div class="grid-body no-border" style="display: block;">
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        {!! Form::model($banner, ['method' => 'post']) !!}
                            <div class="form-group">
                                <label class="form-label">Слоган</label>
                                <span class="help">2 слова через пробел</span>
                                <div class="controls">
                                    {!! Form::text('alt', null, ['class' => 'form-control' . (!$errors->has('alt') ? '' : ' error') ]) !!}
                                    @if ($errors->has('alt'))
                                        <label class="error" for="alt">{{ $errors->first('alt') }}</label>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="form-label">Изображение</label>
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                                <i class="fa fa-picture-o"></i> Выбрать
                                            </a>
                                        </span>
                                        <input id="thumbnail" class="form-control" type="text" name="image"
                                        readonly value="{{ $banner->image ?? '' }}">
                                    </div>
                                    @if ($errors->has('image'))
                                        <label class="error" for="image">{{ $errors->first('image') }}</label>
                                    @endif
                                    <div class='close-image'>
                                        <button type='button' id="delImg" class='close'>×</button>
                                        <img id="holder" src="{{ $banner->image ?? '' }}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="form-label">Ссылка</label>
                                <div class="controls">
                                    {!! Form::text('link', null, ['class' => 'form-control' . (!$errors->has('link') ? '' : ' error') ]) !!}
                                    @if ($errors->has('link'))
                                        <label class="error" for="link">{{ $errors->first('link') }}</label>
                                    @endif
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success btn-cons">Сохранить</button>
                        {!! Form::close() !!}
                        @include('backend.partials.filemanager')
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop
