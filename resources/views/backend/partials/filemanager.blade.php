@push('css')
    <style type="text/css">
        div.modal-dialog { width: 65%; }
        div.modal-content { height: 600px; }
        div.close-image { display:inline-block; }
        iframe { width: 100%; height: 100%; border: 0; }
        div.close-image img { margin-top:15px;max-height:100px; }
    </style>
@endpush

@push('js')
    <script type="text/javascript">
        // File Manager For Images
        var route_prefix = "{{ url(config('lfm.prefix')) }}";
        {!! \File::get('vendor/laravel-filemanager/js/lfm.js') !!}

        var filemanager = {
            'set': function(id) {         
                $('#' + id).on('click', function() {
                    $("#lfm-modal-dialog").modal('show');
                }).filemanager('image', {prefix: route_prefix, modal: {iframe: 'filemanager', dialog: 'lfm-modal-dialog'}});
            },
            'setImageDelButton': function(button) {
                button = typeof button !== 'undefined' ? button : 'button.close';

                var $btnClose = $(button);

                $btnClose.hide();
                $btnClose.on('click', function() { 
                    $this = $(this);
                    $this.parent().prev().children('input').val('');
                    $this.next('img').attr('src', '');
                    $this.hide();
                });

                $btnClose.parent().on('mouseover', function() {
                    $btnClose.show();
                }).on('mouseout', function() {
                    $btnClose.hide();
                });
            }
        }
    </script>
@endpush

<div class="modal fade" id="lfm-modal-dialog" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <iframe id="filemanager"></iframe>
        </div>
    </div>
</div>