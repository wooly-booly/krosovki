@push('js')
    <script type="text/javascript">
        function setDelEvent(selector) {
            $(document).on('click', selector, function(e) {
                e.preventDefault();
                var $this = $(this);
                var data = {
                    url: $this.attr('href'),
                    method: $this.data('method')
                };

                $("#destroyBtn").click(data, function(e) {
                    var $this = $(this);
                    $("#destroyModal").modal('hide');

                    $.ajax({
                        type: data.method,
                        url: data.url,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    }).done(function (data) {
                        location.reload();
                    });
                });

                $("#destroyModal").modal('show');
            });
        }
    </script>
@endpush

<div class="modal fade in" id="destroyModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <br>
                <h4 id="myModalLabel" class="semi-bold">Вы уверены, что хотите удалить эту запись?</h4>
                <br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Отмена</button>
                <button type="button" id="destroyBtn" class="btn btn-danger">Удалить</button>
            </div>
        </div>
    </div>
</div>