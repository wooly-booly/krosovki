@if ($breadcrumbs)
    <ul class="breadcrumb">
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url && !$breadcrumb->last)
                <li><a class="text-uppercase" href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
            @else
                <li><a class="active">{{ $breadcrumb->title }}</a></li>
            @endif
        @endforeach
    </ul>
    <br>
@endif