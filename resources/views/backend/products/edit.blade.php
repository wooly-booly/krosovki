@extends('backend.base')

@section('breadcrumbs', Breadcrumbs::render('products.edit', $product))
@section('page-title', 'Редактировать товар')

@section('content')
    <!-- BEGIN PlACE PAGE CONTENT HERE -->
    <div class="row">
        <div class="col-md-12">
            @include('backend.products.form')
        </div>
    </div>
    <!-- END PLACE PAGE CONTENT HERE -->
@stop