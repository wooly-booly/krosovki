@push('css')
	{{-- HTML5 WYSIYG --}}
	<link href="/_admin/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css">
@endpush

@push('js')
	{{-- HTML5 WYSIYG --}}
	<script src="/_admin/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
	<script src="/_admin/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
	{{-- Select with Autocomplete --}}
	<script src="/_admin/assets/js/select-autocomplete.js" type="text/javascript"></script>

	<script type="text/javascript">
	    var image_row = 1;
		function addImage(img) {
			img = img || {image: '', position: ''};

			html = '<tr id="image-row' + image_row + '">';
			// Choose image
			html += '<td><div class="controls"><div class="input-group"><span class="input-group-btn"><a id="lfm' + image_row + '" data-input="thumbnail' + image_row + '" data-preview="holder' + image_row + '" class="btn btn-primary"><i class="fa fa-picture-o"></i> Выбрать</a></span><input id="thumbnail' + image_row + '" class="form-control" type="text" name="product_image[' + image_row + '][image]" readonly value="' + img.image + '"></div></div></td>';
			// Position
			html += '<td class="text-right"><input type="text" name="product_image[' + image_row + '][position]" placeholder="Позиция" class="form-control" value="' + img.position + '" /></td>';
			// Thumbnail
			html += '<td><div class="close-image"><button type="button" onclick="$(\'#image-row' + image_row  + '\').remove();" class="close">×</button><img id="holder' + image_row + '" src="' + img.image + '"></div></td>';
			html += '</tr>';

			$('#images tbody').append(html);

			filemanager.set('lfm' + image_row);

			image_row++;
		}

		$(document).ready(function(){
			$('#text-editor').wysihtml5();

			// Product Form Tabs
			$('#ProductFormTab a').click(function (e) {
				e.preventDefault();
				$(this).tab('show');
			});
			// Product Status
			$("#status").select2({minimumResultsForSearch: Infinity});
			// Select Category For Product
			var categories = $("#categories");
			autocomplete(categories, '{!! route('categories.autocomplete') !!}');
			// Select Brand For Product
			var brand = $("#brand");
			autocomplete(brand, '{!! route('brands.autocomplete') !!}');
			// Set Images For Product
			var imgs = {!! $product->exists ? $product->images->toJson() : 'null' !!};
			if (imgs) {
				for (var i=0; i < imgs.length; i++) {
					addImage(imgs[i]);
				}
			}
			// Set Filemanager
			filemanager.set('lfm');
			filemanager.setImageDelButton('#delImg');
		});
	</script>
@endpush

{!! Form::model($product, [
	'method' => $product->exists ? 'put' : 'post',
	'route' => $product->exists ? ['products.update', $product->id] : ['products.store'],
	'enctype' => 'multipart/form-data'
]) !!}

<ul class="nav nav-tabs" id="ProductFormTab">
	<li class="active"><a href="#tab-general">Основное</a></li>
	<li><a href="#tab-data">Данные</a></li>
	<li><a href="#tab-links">Связи</a></li>
	<li><a href="#tab-images">Изображения</a></li>
</ul>
 
<div class="tab-content">
	{{-- Tab General --}}
	<div class="tab-pane active" id="tab-general">
		<div class="form-group">
			<label class="form-label">Название</label>
			<div class="controls">
				{!! Form::text('description[title]', null, ['class' => 'form-control' . (!$errors->has('description.title') ? '' : ' error') ]) !!}
				@if ($errors->has('description.title'))
					<label class="error" for="description[title]">{{ $errors->first('description.title') }}</label>
				@endif
			</div>
		</div>

		<div class="form-group {{ !$errors->has('description.description') ?: 'has-error'  }}">
			<label class="form-label">Описание</label>
			<div class="controls">
				{!! Form::textarea('description[description]', null, ['class' => 'form-control', 'id' => 'text-editor', 'rows' => '7']) !!}
			</div>
			@if ($errors->has('description.description'))
				<label class="error" for="description[description]">{{ $errors->first('description.description') }}</label>
			@endif
		</div>

		<div class="form-group {{ !$errors->has('description.meta_description') ?: 'has-error'  }}">
			<label class="form-label">Метатег Описание</label>
			<div class="controls">
				{!! Form::textarea('description[meta_description]', null, ['class' => 'form-control', 'rows' => '2']) !!}
			</div>
			@if ($errors->has('description.meta_description'))
				<label class="error" for="description[meta_description]">{{ $errors->first('description.meta_description') }}</label>
			@endif
		</div>

		<div class="form-group {{ !$errors->has('description.meta_keyword') ?: 'has-error'  }}">
			<label class="form-label">Метатег Ключевые слова</label>
			<div class="controls">
				{!! Form::textarea('description[meta_keyword]', null, ['class' => 'form-control', 'rows' => '2']) !!}
			</div>
			@if ($errors->has('description.meta_keyword'))
				<label class="error" for="description[meta_keyword]">{{ $errors->first('description.meta_keyword') }}</label>
			@endif
		</div>
	</div>

	{{-- Tab Data --}}
	<div class="tab-pane" id="tab-data">
		<div class="form-group">
			<label class="form-label">Изображение</label>
			<div class="controls">
				<div class="input-group">
					<span class="input-group-btn">
						<a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
							<i class="fa fa-picture-o"></i> Выбрать
						</a>
					</span>
					<input id="thumbnail" class="form-control" type="text" name="image"
					readonly value="{{ $product->exists ? $product->image : '' }}">
				</div>
				<div class='close-image'>
					<button type='button' id="delImg" class='close'>×</button>
					<img id="holder" src="{{ $product->exists ? $product->image : '' }}">
				</div>
			</div>
			@if ($errors->has('image'))
				<label class="error" for="image">{{ $errors->first('image') }}</label>
			@endif 
		</div>

		<div class="form-group">
			<label class="form-label">Цена</label>
			<span class="help">пример: 100.00</span>
			<div class="controls">
				{!! Form::text('price', null, ['class' => 'form-control' . (!$errors->has('price') ? '' : ' error') ]) !!}
				@if ($errors->has('price'))
					<label class="error" for="price">{{ $errors->first('price') }}</label>
				@endif
			</div>
		</div>

		<div class="form-group">
			<label class="form-label">Url-ссылка</label>
			<span class="help">пример: "то-что-будет-в-адресной-строке"</span>
			<div class="controls">
				{!! Form::text('url_key', null, ['class' => 'form-control' . (!$errors->has('url_key') ? '' : ' error') ]) !!}
				@if ($errors->has('url_key'))
					<label class="error" for="url_key">{{ $errors->first('url_key') }}</label>
				@endif
			</div>
		</div>

		<div class="form-group">
			<label class="form-label">Статус</label>
			<div class="controls">
				{!! Form::select('status', ['1'=>'Включено', '0'=>'Отключено'], $product->status, ['id' => 'status', 'style' => 'width: 100%']) !!}
			</div>
		</div>

		<div class="form-group">
			<label class="form-label">Позиция</label>
			<span class="help">для сортировки</span>
			<div class="controls">
				{!! Form::text('position', null, ['class' => 'form-control' . (!$errors->has('position') ? '' : ' error') ]) !!}
				@if ($errors->has('position'))
					<label class="error" for="position">{{ $errors->first('position') }}</label>
				@endif
			</div>
		</div>
 
	</div>

	{{-- Tab Links --}}
	<div class="tab-pane" id="tab-links">
		<div class="form-group">
			<label class="form-label">Производитель</label>
			<span class="help">автозаполнение</span>
			<div class="controls">
				<select id="brand" name="brand_id" style="width: 100%">
					@if ($product->exists && $product->brand_id)
						<option selected="selected" value="{{ $product->brand_id }}">
							{{ $product->brand->title }}
						</option>
					@endif
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="form-label">Категории</label>
			<span class="help">автозаполнение</span>
			<div class="controls">
				<select id="categories" name="category_ids[]" multiple="multiple" style="width: 100%">
					@if ($product->exists && $product->categoriesById)
						@foreach ($product->categoriesById as $id => $cat)
							<option selected="selected" value="{{ $id }}">
								{{ $cat }}
							</option>
						@endforeach
					@endif
				</select>
			</div>
		</div>
	</div>

	{{-- Tab Images --}}
	<div class="tab-pane" id="tab-images">
		<div class="table-responsive">
			<table id="images" class="table table-bordered table-hover">
				<thead>
					<tr>
						<td style="width: 72%">Изображение</td>
						<td style="width: 14%">Позиция</td>
						<td style="width: 14%"></td>
					</tr>
				</thead>
				<tbody>

				</tbody>
				<tfoot>
					<tr>
						<td colspan="2"></td>
						<td class="text-left">
							<button type="button" onclick="addImage();" data-toggle="tooltip" title="" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>

<button type="submit" class="btn btn-success btn-cons">{{ $product->exists ? 'Обновить' : 'Сохранить' }}</button>
{!! Form::close() !!}

@include('backend.partials.filemanager')
