@extends('backend.base')

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@push('css')
    <link href="/_admin/assets/plugins/jquery-datatables/datatables.min.css" rel="stylesheet" type="text/css">
    <link href="/_admin/assets/plugins/jquery-datatable/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
    <style>
        #products table.table td {vertical-align: middle;}
        #products table.table img {width: 75px;}
    </style>
@endpush

@push('js')
    <script src="/_admin/assets/plugins/jquery-datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/_admin/assets/js/datatable.js" type="text/javascript"></script>

    <script type="text/javascript">

        $(document).ready( function () {
            // Product's DataTable
            $('#products .table').dataTable({
                "dom": "<'row'<'col-md-6'l <'toolbar'>><'col-md-6'f>r>t<'row'<'col-md-12'p i>>",
                "language": <?php include('_admin/assets/plugins/jquery-datatables/datatables.lang.Russian.json'); ?>,
                "order": [[ 2, "asc" ]],
                "processing": true,
                "serverSide": true,
                "ajax": '{!! route('products.datatable') !!}',
                "columns": [{
                        "data": "id",
                        "sortable": false,
                        "searchable": false,
                        "render": function (id, type, full)  {
                            return '<div class="checkbox check-default"><input value="'+id+'" id="checkbox'+id+'" type="checkbox"><label for="checkbox'+id+'"></label></div>';
                        }
                    },{
                        "data": "image",
                        "sortable": false,
                        "searchable": false,
                        "class": "text-center",
                        "render": function (image, type, full)  {
                            if (image) {
                                return '<img src="' + image + '" class="img-thumbnail">';
                            } else {
                                return '';
                            }
                        }
                    },{
                        "data": "description.title",
                    },{
                        "data": "price",
                    },{
                        "data": "status",
                        "searchable": false,
                        "class": "text-center"
                    },{
                        "data": "id",
                        "searchable": false,
                        "sortable": false,
                        "class": "text-right",
                        "render": function (id, type, full)  {
                            var linkDelete = '{!! route("products.destroy", ["products" => '#id_delete']) !!}';
                            var linkEdit = '{!! route("products.edit", ["products" => '#id_edit']) !!}';
                            linkDelete = linkDelete.replace("#id_delete", id);
                            linkEdit = linkEdit.replace("#id_edit", id);
                             
                            return '<a href="'+ linkEdit +'" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a><a href="'+ linkDelete +'" data-toggle="tooltip" data-method="delete" class="postback btn btn-danger" data-original-title="Удалить"><i class="fa fa-trash-o"></i></a>';
                        }
                    }
                ]
            });

            $("div.toolbar").html('<div class="table-tools-actions"><a class="btn btn-primary" href="{{ route("products.create") }}" style="margin-left:12px">Добавить</a></div>');

            $('#products .dataTables_filter input').addClass("input-medium ");
            $('#products .dataTables_length select').removeClass().addClass("select2-wrapper span12");
            $(".select2-wrapper").select2({minimumResultsForSearch: -1});

            // Delete Product
            $(document).on('click', 'a.postback', function(e) {
                e.preventDefault();
                var $this = $(this);
                var data = {
                    url: $this.attr('href'),
                    method: $this.data('method')
                };

                $("#delProduct").click(data, function(e) {
                    var $this = $(this);
                    $("#deleteProductModal").modal('hide');

                    $.ajax({
                        type: data.method,
                        url: data.url,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    }).done(function (data) {
                        location.reload();
                    });
                });

                $("#deleteProductModal").modal('show');
            });
        });
    </script>
@endpush

@section('breadcrumbs', Breadcrumbs::render('products.index'))
@section('page-title', 'Товары')

@section('content')
    <div class="row-fluid">
        <div class="span12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4>Cписок товаров</h4>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="grid-body ">
                    <div id="products">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width: 2%"></th>
                                            <th style="width: 4%">Изображение</th>
                                            <th style="width: 58%">Название товара</th>
                                            <th style="width: 5%" class="text-center">Цена</th>
                                            <th style="width: 5%" class="text-center">Статус</th>
                                            <th style="width: 13%" class="text-right">Действие</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade in" id="deleteProductModal" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <br>
                                    <h4 id="myModalLabel" class="semi-bold">Вы уверены что хотите удалить этот товар?</h4>
                                    <br>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-info" data-dismiss="modal">Отмена</button>
                                    <button type="button" id="delProduct" class="btn btn-danger">Удалить</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop