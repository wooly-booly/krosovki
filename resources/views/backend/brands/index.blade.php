@extends('backend.base')

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@push('css')
    <link href="/_admin/assets/plugins/jquery-datatables/datatables.min.css" rel="stylesheet" type="text/css">
    <link href="/_admin/assets/plugins/jquery-datatable/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
@endpush

@push('js')
    <script src="/_admin/assets/plugins/jquery-datatables/datatables.min.js" type="text/javascript"></script>
    <script src="/_admin/assets/js/datatable.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready( function () {
            $('#brands .table').dataTable({
                "dom": "<'row'<'col-md-6'l <'toolbar'>><'col-md-6'f>r>t<'row'<'col-md-12'p i>>",
                "language": <?php include('_admin/assets/plugins/jquery-datatables/datatables.lang.Russian.json'); ?>,
                "order": [[ 1, "asc" ]],
                "processing": true,
                "serverSide": true,
                "ajax": '{!! route('brands.datatable') !!}',
                "columns": [{
                        "data": "id",
                        "name": "id",
                        "sortable": false,
                        "searchable": false,
                        "render": function (id, type, full)  {
                            return '<div class="checkbox check-default"><input value="'+id+'" id="checkbox'+id+'" type="checkbox"><label for="checkbox'+id+'"></label></div>';
                        }
                    },{
                        "data": "title",
                        "name": "title"
                    },{
                        "data": "position",
                        "name": "position",
                        "searchable": false,
                        "class": "text-center"
                    },{
                        "data": "id",
                        "name": "id",
                        "searchable": false,
                        "sortable": false,
                        "class": "text-center",
                        "render": function (id, type, full)  {
                            var linkDelete = '{!! route("brands.destroy", ["brand" => '#id_delete']) !!}';
                            var linkEdit = '{!! route("brands.edit", ["brand" => '#id_edit']) !!}';
                            linkDelete = linkDelete.replace("#id_delete", id);
                            linkEdit = linkEdit.replace("#id_edit", id);
                             
                            return '<a href="'+ linkEdit +'" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редактировать"><i class="fa fa-pencil"></i></a><a href="'+ linkDelete +'" data-toggle="tooltip" data-method="delete" class="postback btn btn-danger" data-original-title="Удалить"><i class="fa fa-trash-o"></i></a>';
                        }
                    },
                ]
            });

            $("div.toolbar").html('<div class="table-tools-actions"><a class="btn btn-primary" href="{{ route("brands.create") }}" style="margin-left:12px">Добавить</a></div>');

            $('#brands .dataTables_filter input').addClass("input-medium ");
            $('#brands .dataTables_length select').removeClass().addClass("select2-wrapper span12");
            $(".select2-wrapper").select2({minimumResultsForSearch: -1});

            // Delete brand
            $(document).on('click', 'a.postback', function(e) {
                e.preventDefault();
                var $this = $(this);
                var data = {
                    url: $this.attr('href'),
                    method: $this.data('method')
                };

                $("#delBrand").click(data, function(e) {
                    var $this = $(this);
                    $("#deleteBrandModal").modal('hide');

                    $.ajax({
                        type: data.method,
                        url: data.url,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    }).done(function (data) {
                        location.reload();
                    });
                });

                $("#deleteBrandModal").modal('show');
            });
        });
    </script>
@endpush

@section('breadcrumbs', Breadcrumbs::render('brands.index'))
@section('page-title', 'Бренды')

@section('content')
    <div class="row-fluid">
        <div class="span12">
            <div class="grid simple ">
                <div class="grid-title">
                    <h4>Cписок брендов</h4>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="grid-body ">
                    <div id="brands">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th style="width: 2%"></th>
                                            <th style="width: 55%">Название</th>
                                            <th style="width: 5%" class="text-center">Позиция</th>
                                            <th style="width: 10%" class="text-center">Действие</th>
                                        </tr>
                                    </thead>
                                </table>

                            </div>
                        </div>
                    </div>
                    <div class="modal fade in" id="deleteBrandModal" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <br>
                                    <h4 id="myModalLabel" class="semi-bold">Вы уверены, что хотите удалить этот бренд?</h4>
                                    <br>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-info" data-dismiss="modal">Отмена</button>
                                    <button type="button" id="delBrand" class="btn btn-danger">Удалить</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop