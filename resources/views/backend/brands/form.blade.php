@push('js')
    <script type="text/javascript">
        $(document).ready( function () {
            // Set Filemanager
            filemanager.set('lfm');
            filemanager.setImageDelButton('#delImg');
        });
    </script>
@endpush

{!! Form::model($brand, [
    'method' => $brand->exists ? 'put' : 'post',
    'route' => $brand->exists ? ['brands.update', $brand->id] : ['brands.store'],
    'enctype' => 'multipart/form-data'
]) !!}

<div class="form-group">
    <label class="form-label">Название</label>
    <div class="controls">
        {!! Form::text('title', null, ['class' => 'form-control' . (!$errors->has('title') ? '' : ' error') ]) !!}
        @if ($errors->has('title'))
            <label class="error" for="title">{{ $errors->first('title') }}</label>
        @endif
    </div>
</div>

<div class="form-group">
    <label class="form-label">Url-ссылка</label>
    <span class="help">пример: "то-что-будет-в-адресной-строке"</span>
    <div class="controls">
        {!! Form::text('url_key', null, ['class' => 'form-control' . (!$errors->has('url_key') ? '' : ' error') ]) !!}
        @if ($errors->has('url_key'))
            <label class="error" for="url_key">{{ $errors->first('url_key') }}</label>
        @endif
    </div>
</div>

<div class="form-group">
    <label class="form-label">Изображение</label>
    <div class="controls">
        <div class="input-group">
            <span class="input-group-btn">
                <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                    <i class="fa fa-picture-o"></i> Выбрать
                </a>
            </span>
            <input id="thumbnail" class="form-control" type="text" name="image"
            readonly value="{{ $brand->exists ? $brand->image : '' }}">
        </div>
        <div class='close-image'>
            <button type='button' id="delImg" class='close'>×</button>
            <img id="holder" src="{{ $brand->exists ? $brand->image : '' }}">
        </div>
    </div>
    @if ($errors->has('image'))
        <label class="error" for="image">{{ $errors->first('image') }}</label>
    @endif 
</div>

<div class="form-group">
    <label class="form-label">Позиция</label>
    <span class="help">для сортировки</span>
    <div class="controls">
        {!! Form::text('position', null, ['class' => 'form-control' . (!$errors->has('position') ? '' : ' error') ]) !!}
        @if ($errors->has('position'))
            <label class="error" for="position">{{ $errors->first('position') }}</label>
        @endif
    </div>
</div>

<button type="submit" class="btn btn-success btn-cons">{{ $brand->exists ? 'Обновить' : 'Сохранить' }}</button>
{!! Form::close() !!}

@include('backend.partials.filemanager')