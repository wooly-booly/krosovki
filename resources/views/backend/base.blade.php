<!DOCTYPE html>
<html>
<head>
    <title>Admin Panel</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    @yield('meta')

    <!-- BEGIN PLUGIN CSS -->
    <link href="/_admin/assets/plugins/select2-4.0.3/css/select2.min.css" rel="stylesheet" type="text/css" media="screen">
    <link href="/_admin/assets/plugins/select2-4.0.3/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" media="screen">
    <link href="/_admin/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="/_admin/assets/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/_admin/assets/plugins/bootstrapv3/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="/_admin/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="/_admin/assets/plugins/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="/_admin/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" />
    <!-- END PLUGIN CSS -->

    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/_admin/webarch/css/webarch.css" rel="stylesheet" type="text/css" />
    <link href="/_admin/assets/css/backend.css" rel="stylesheet" type="text/css" />
    <!-- END CORE CSS FRAMEWORK -->

    <!-- ADDITIONAL CSS -->
    @stack('css')
    <!-- END ADDITIONAL CSS -->

</head>
<body class="">
<!-- BEGIN CONTENT -->
<div class="page-container row-fluid">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar " id="main-menu">
        <!-- BEGIN MINI-PROFILE -->
        <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
            <div class="user-info-wrapper sm">
                <div class="profile-wrapper sm">
                    <img src="/_admin/assets/img/profiles/avatar.jpg" alt="" width="69" height="69" />
                    <div class="availability-bubble online"></div>
                </div>
                <div class="user-info sm">
                    <div class="username">Sheriff-<span class="semi-bold">Shop</span></div>
                    <div class="status">Терпение и труд!</div>
                </div>
            </div>
            <!-- END MINI-PROFILE -->
            <!-- BEGIN SIDEBAR MENU -->
            <p class="menu-title sm">Навигация{{-- <span class="pull-right"><a href="javascript:;"><i class="material-icons">refresh</i></a></span> --}}</p>
            <ul>
                <li class="start">
                    <a href="{{ route('products.index') }}">
                        <i class="material-icons">monetization_on</i>
                        <span class="title">Товары</span>
                    </a>
                </li>
                <li class="start">
                    <a href="{{ route('categories.index') }}">
                        <i class="material-icons">format_list_bulleted</i>
                        <span class="title">Категории</span>
                    </a>
                </li>
                <li class="start">
                    <a href="{{ route('brands.index') }}">
                        <i class="material-icons">check_circle</i>
                        <span class="title">Бренды</span>
                    </a>
                </li>
                <li class="start">
                    <a href="{{ route('pages.index') }}">
                        <i class="material-icons">receipt</i>
                        <span class="title">Страницы</span>
                    </a>
                </li>
                <li class="start">
                    <a href="{{ route('articles.index') }}">
                        <i class="material-icons">library_books</i>
                        <span class="title">Статьи</span>
                    </a>
                </li>
                <li class="start">
                    <a href="{{ route('settings.index') }}">
                        <i class="material-icons">settings</i>
                        <span class="title">Настройки</span>
                    </a>
                </li>
                <li class="">
                    <a href=""> <i class="material-icons">extension</i> <span class="title">Модули</span> <span class=" arrow"></span> </a>
                    <ul class="sub-menu">
                        <li>
                            <a href=""> <span class="title">Баннеры</span> <span class=" arrow"></span> </a>
                            <ul class="sub-menu">
                                <li><a href="{{ route('banner.2words') }}">2 Words</a></li>
                                <li><a href="{{ route('banner.top5') }}">Top 5</a></li>
                                <li><a href="{{ route('banner.carousel') }}">Carousel</a></li>
                                <li><a href="{{ route('banner.collection') }}">Collection</a></li>
                            </ul>
                        </li>
                        {{-- <li> <a href="javascript:;">To be...</a> </li> --}}
                    </ul>
                </li>
                <li class="hidden-lg hidden-md hidden-xs" id="more-widgets">
                    <a href="javascript:;"> <i class="material-icons"></i></a>
                    <ul class="sub-menu">
                        <li class="side-bar-widgets">
                            <p class="menu-title sm">FOLDER <span class="pull-right"><a href="#" class="create-folder"><i class="material-icons">add</i></a></span></p>
                            <ul class="folders">
                                <li>
                                    <a href="#">
                                        <div class="status-icon green"></div>
                                        My quick tasks </a>
                                </li>
                            </ul>
                            <p class="menu-title">PROJECTS </p>
                            <div class="status-widget">
                                <div class="status-widget-wrapper">
                                    <div class="title">Freelancer<a href="#" class="remove-widget"><i class="material-icons">close</i></a></div>
                                    <p>Redesign home page</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <a href="#" class="scrollup">Scroll</a>
    <!-- END SIDEBAR -->

    <!-- BEGIN PAGE CONTAINER-->
    <div class="page-content">
        <div class="content">
            <!-- BEGIN Breadcrumbs -->
            @yield('breadcrumbs')
            <!-- END Breadcrumbs -->

            <!-- BEGIN Messages & Notifications -->
            @if (Session::has('success'))
                <div class="alert alert-success">
                    <button class="close" data-dismiss="alert"></button>
                    {{ Session::get('success') }}
                </div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-error">
                    <button class="close" data-dismiss="alert"></button>
                    {{ Session::get('error') }}
                </div>
            @endif
            <!-- END Messages & Notifications -->

            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <i class="icon-custom-left"></i>
                <h3>@yield('page-title')</h3>
            </div>
            <!-- END PAGE TITLE -->

            <!-- BEGIN Content -->
            @yield('content')
            <!-- END Content -->
        </div>
    </div>
    <!-- END PAGE CONTAINER -->
</div>
<!-- END CONTENT -->

<!-- BEGIN CORE JS FRAMEWORK-->
<script src="/_admin/assets/plugins/pace/pace.min.js" type="text/javascript"></script>

<!-- BEGIN JS DEPENDECENCIES-->
<script src="/_admin/assets/plugins/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="/_admin/assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/_admin/assets/plugins/jquery-block-ui/jqueryblockui.min.js" type="text/javascript"></script>
<script src="/_admin/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="/_admin/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js" type="text/javascript"></script>
<script src="/_admin/assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js" type="text/javascript"></script>
<script src="/_admin/assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/_admin/assets/plugins/select2-4.0.3/js/select2.min.js" type="text/javascript"></script>
<script src="/_admin/assets/plugins/select2-4.0.3/js/i18n/ru.js" type="text/javascript"></script>
<!-- END CORE JS DEPENDECENCIES-->

<!-- BEGIN CORE TEMPLATE JS -->
<script src="/_admin/webarch/js/webarch.js" type="text/javascript"></script>
<script src="/_admin/assets/js/chat.js" type="text/javascript"></script>
<!-- END CORE TEMPLATE JS -->

<!-- ADDITIONAL JS -->
@stack('js')
<!-- END ADDITIONAL JS -->
</body>
</html>