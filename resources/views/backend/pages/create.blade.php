@extends('backend.base')

@section('breadcrumbs', Breadcrumbs::render('pages.create'))
@section('page-title', 'Добавить страницу')

@section('content')
    <!-- BEGIN PlACE PAGE CONTENT HERE -->
    <div class="row">
        <div class="col-md-12">
            @include('backend.pages.form')
        </div>
    </div>
    <!-- END PLACE PAGE CONTENT HERE -->
@stop
