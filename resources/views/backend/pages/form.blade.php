@push('css')
    {{-- HTML5 WYSIYG --}}
    <link href="/_admin/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css">
@endpush

@push('js')
    {{-- HTML5 WYSIYG --}}
    <script src="/_admin/assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
    <script src="/_admin/assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#text-editor').wysihtml5();      
            // Тabs
            $('#pageTab a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });
            //Page Status
            $("#status").select2({minimumResultsForSearch: Infinity});
        }); 
    </script>
@endpush

{!! Form::model($page, [
    'method' => $page->exists ? 'put' : 'post',
    'route' => $page->exists ? ['pages.update', $page->id] : ['pages.store']
]) !!}

<ul class="nav nav-tabs" id="pageTab">
    <li class="active"><a href="#tab-general">Основное</a></li>
    <li><a href="#tab-data">Данные</a></li>
</ul>

<div class="tab-content">
    {{-- Tab General --}}
    <div class="tab-pane active" id="tab-general">
        <div class="form-group">
            <label class="form-label">Название</label>
            <div class="controls">
                {!! Form::text('description[title]', null, ['class' => 'form-control' . (!$errors->has('description.title') ? '' : ' error') ]) !!}
                @if ($errors->has('description.title'))
                    <label class="error" for="description[title]">{{ $errors->first('description.title') }}</label>
                @endif
            </div>
        </div>

        <div class="form-group {{ !$errors->has('description.content') ?: 'has-error'  }}">
            <label class="form-label">Содержание</label>
            <div class="controls">
                {!! Form::textarea('description[content]', null, ['class' => 'form-control', 'id' => 'text-editor', 'rows' => '7']) !!}
            </div>
            @if ($errors->has('description.content'))
                <label class="error" for="description[content]">{{ $errors->first('description.content') }}</label>
            @endif
        </div>

        <div class="form-group {{ !$errors->has('description.meta_description') ?: 'has-error'  }}">
            <label class="form-label">Метатег Описание</label>
            <div class="controls">
                {!! Form::textarea('description[meta_description]', null, ['class' => 'form-control', 'rows' => '2']) !!}
            </div>
            @if ($errors->has('description.meta_description'))
                <label class="error" for="description[meta_description]">{{ $errors->first('description.meta_description') }}</label>
            @endif
        </div>

        <div class="form-group {{ !$errors->has('description.meta_keyword') ?: 'has-error'  }}">
            <label class="form-label">Метатег Ключевые слова</label>
            <div class="controls">
                {!! Form::textarea('description[meta_keyword]', null, ['class' => 'form-control', 'rows' => '2']) !!}
            </div>
            @if ($errors->has('description.meta_keyword'))
                <label class="error" for="description[meta_keyword]">{{ $errors->first('description.meta_keyword') }}</label>
            @endif
        </div>
    </div>

    {{-- Tab Data --}}
    <div class="tab-pane" id="tab-data">
        <div class="form-group">
            <label class="form-label">Url-ссылка</label>
            <span class="help">пример: "то-что-будет-в-адресной-строке"</span>
            <div class="controls">
                {!! Form::text('url_key', null, ['class' => 'form-control' . (!$errors->has('url_key') ? '' : ' error') ]) !!}
                @if ($errors->has('url_key'))
                    <label class="error" for="url_key">{{ $errors->first('url_key') }}</label>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label class="form-label">Статус</label>
            <div class="controls">
                {!! Form::select('status', ['1'=>'Включено', '0'=>'Отключено'], $page->status, ['id' => 'status', 'style' => 'width: 100%']) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="form-label">Позиция</label>
            <span class="help">для сортировки</span>
            <div class="controls">
                {!! Form::text('position', null, ['class' => 'form-control' . (!$errors->has('position') ? '' : ' error') ]) !!}
                @if ($errors->has('position'))
                    <label class="error" for="position">{{ $errors->first('position') }}</label>
                @endif
            </div>
        </div>
    </div>
</div>

{!! Form::submit($page->exists ? 'Обновить' : 'Сохранить', ['class' => 'btn btn-primary']) !!}

{!! Form::close() !!}