<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('page-title', $settings->config['store_title'] ?? 'Sherriff-Shop')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('meta')
    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="/img/settings/icon.png">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->
    <!-- google fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,900,700,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Bree+Serif' rel='stylesheet' type='text/css'>

    <!-- all css here:
        bootstrap v3.3.6, animate, pe-icon-7-stroke, pe-icon-7-stroke, jquery-ui.min,
        meanmenu, nivo.slider, owl.carousel, style, responsive
    -->
    <link rel="stylesheet" href="{{ elixir('css/site.css') }}">

    <!-- additional css-->
    @stack('css')
    <!-- end additional css -->

    <!-- modernizr js -->
    <script src="{{ elixir('js/vendor/modernizr-2.8.3.min.js') }}"></script>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- header & pages-title sections start -->
@include('store.common.header')
<!-- header & pages-title sections end -->

<!-- content-start -->
@yield('content')
<!-- content-end -->

<!-- footer section start -->
@include('store.common.footer')
<!-- footer section end -->

<!-- all js here -->
<!-- jquery latest version -->
<script src="{{ elixir('js/vendor/jquery-1.12.3.min.js') }}"></script>
<!-- bootstrap js -->
<script src="{{ elixir('js/bootstrap.min.js') }}"></script>
<!-- owl.carousel js -->
<script src="{{ elixir('js/owl.carousel.min.js') }}"></script>
<!-- meanmenu js -->
<script src="{{ elixir('js/jquery.meanmenu.js') }}"></script>
<!-- countdown JS -->
<script src="{{ elixir('js/countdown.js') }}"></script>
<!-- nivo.slider JS -->
<script src="{{ elixir('js/jquery.nivo.slider.pack.js') }}"></script>
<!-- simpleLens JS -->
<script src="{{ elixir('js/jquery.simpleLens.min.js') }}"></script>
<!-- jquery-ui js -->
<script src="{{ elixir('js/jquery-ui.min.js') }}"></script>
<!-- jGrowl js -->
<script src="{{ elixir('js/jquery.jgrowl.min.js') }}"></script>
<!-- load-more js -->
<script src="{{ elixir('js/load-more.js') }}"></script>
<!-- plugins js -->
<script src="{{ elixir('js/plugins.js') }}"></script>
<!-- main js -->
<script src="{{ elixir('js/main.js') }}"></script>

<!-- additional js-->
@stack('js')
<!-- end additional js -->

</html>
