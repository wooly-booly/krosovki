@if (isset($carousel))
<!-- slider-section-start -->
<div class="main-slider-one main-slider-two slider-area">
    <div id="wrapper">
        <div class="slider-wrapper">
            <div id="mainSlider" class="nivoSlider">
                @foreach ($carousel as $item)
                    <a href="{{ $item->link }}"><img src="{{ $item->image }}" alt="{{ $item->alt }}" /></a>
                @endforeach
            </div>
        </div>                          
    </div>
</div>
<!-- slider section end -->
@endif