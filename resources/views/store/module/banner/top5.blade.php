@if (isset($top5))
    <div class="brand-logo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="barnd-bg text-center">
                    @foreach ($top5 as $item)
                        <a href="{{ $item->link }}"><img src="{{ $item->image }}" alt="{{ $item->alt }}" /></a>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif