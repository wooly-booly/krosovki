@if (isset($twoWords))
    <div class="s-side-text">
        <div class="banner clearfix">
            <a href="{{ $twoWords->link }}"><img src="{{ $twoWords->image }}" alt="{{ $twoWords->alt }}"></a>
            <div class="banner-text">
                <h2>best</h2><br>
                <h2 class="banner-brand">brand</h2>
            </div>
        </div>
    </div>
@endif