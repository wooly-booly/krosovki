@if (isset($banner) && $setting->count())
<!-- collection section start -->
<section class="collection-area collection-area2 section-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="single-colect banner collect-one">
                    <a href="{{ $banner[0]['link'] }}"><img src="{{ $banner[0]['image'] }}" alt="{{ $banner[0]['alt'] }}" /></a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="colect-text ">
                    <h4><a href="{{ $banner[0]['link'] }}">{{ $setting['title_left'] }}</a></h4>
                    <h5>{!! $setting['description_left'] !!}</h5>
                    <a href="{{ $banner[0]['link'] }}">Подробнее <i class="mdi mdi-arrow-right"></i></a>
                </div>
                <div class="collect-img banner margin single-colect">
                    <a href="{{ $banner[1]['link'] }}"><img src="{{ $banner[1]['image'] }}" alt="{{ $banner[1]['alt'] }}" /></a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="collect-img banner single-colect">
                    <a href="{{ $banner[2]['link'] }}"><img src="{{ $banner[2]['image'] }}" alt="{{ $banner[2]['alt'] }}" /></a>
                </div>
                <div class="colect-text ">
                    <h4><a href="{{ $banner[1]['link'] }}">{{ $setting['title_right'] }}</a></h4>
                    <p>{!! $setting['description_right'] !!}</p>
                    <a href="{{ $banner[1]['link'] }}">Подробнее <i class="mdi mdi-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- collection section end -->
@endif