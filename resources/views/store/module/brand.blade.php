@inject('brands', 'WBstore\Brand')

@push('css')
    <style>
        .brands-select li {
            border-right: none;
            float: none;
            width: 100%;
            border-bottom: 1px solid #eeecec;
        }
        .brands-select ul {
            overflow: auto;
            max-height: 26vh;
        }
    </style>
@endpush

@if (!empty($brands))
    <div class="s-side-text">
        <div class="sidebar-title clearfix">
            <h4 class="floatleft">Бренды</h4>
            {{-- <h5 class="floatright"><a href="#">All</a></h5> --}}
        </div>
        <div class="brands-select clearfix">
            <ul>
                @foreach ($brands->orderBy('title')->get() as $brand)
                    <li><a href="{{ route('store.brand', ['brand' => $brand->id, 'brand_url_key' => $brand->url_key]) }}">{{ $brand->title }}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
@endif