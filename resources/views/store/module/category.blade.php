@push('css')
	<style>
		.dropdown-submenu {
			position: relative;
		}

		.dropdown-submenu > .dropdown-menu {
			top: 0;
			left: 100%;
			margin-top: -1px;
			/*margin-left: -2px;*/
			box-shadow: none;
			border: 1px solid #eee;
		}

		.dropdown-submenu:hover > .dropdown-menu {
			display: block;
		}

		.dropdown-submenu > a:after {
			content: "";
			margin-top: 16px;
			margin-right: -15px;
			float: right;
			border-left: 4px solid;
			border-top: 4px solid transparent;
			border-bottom: 4px solid transparent;
		}
		
		.dropdown-menu {
			padding: 0;
			border-radius: 4px;
		}

		.normal-a > a {
			padding-left: 30px;
			font-weight: 500;
			line-height: 40px;
			text-transform: uppercase;
		}
	</style>
@endpush

<div class="s-side-text">
	<div class="sidebar-title clearfix">
		<h4 class="floatleft">Категории</h4>
	</div>
	<div class="categories left-right-p">
		@if (count($sidebarCategoryItems) > 0)
		    <div class="normal-a">
			    @foreach ($sidebarCategoryItems as $item)
			        @include('store.partials.sidebar_category_menu', $item)
			    @endforeach
		    </div>
		@else
		    <div class="normal-a"><a href="#">Пока, нет категорий!</a></div>
		@endif
	</div>
</div>
