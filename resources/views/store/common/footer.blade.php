<footer class="footer-two">
    <!-- top5 logo area start -->
    @include('store.module.banner.top5')
    <!-- top5 logo section end -->
    <!-- social media section start -->
    <div class="social-media section-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="newsletter newsletter2">
                        <h3>newsletter</h3>
                        <form action="mail.php" method="post">
                            <input type="text" name="email" placeholder="Enter your email..."/>
                            <input type="submit" value="subscribe"/>
                        </form>
                    </div>
                    <div class="social-icons">
                        <a href="#"><i class="mdi mdi-facebook"></i></a>
                        <a href="#"><i class="mdi mdi-twitter"></i></a>
                        <a href="#"><i class="mdi mdi-google-plus"></i></a>
                        <a href="#"><i class="mdi mdi-dribbble"></i></a>
                        <a href="#"><i class="mdi mdi-rss"></i></a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-offset-1 col-md-5">
                    <div class="newsletter get-touch">
                        <h3>get in touch</h3>
                        <form action="mail.php" method="post">
                            <input type="text" name="name" placeholder="Enter your Name..."/>
                            <input type="text" name="email" placeholder="Enter your email..."/>
                            <textarea name="message" rows="2" placeholder="Enter your message...."></textarea>
                            <input type="submit" value="send your message"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- social media section end -->
    <!-- footer-top area start -->
    <div class="footer-top section-padding">
        <div class="footer-dsc">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="single-text">
                            <div class="footer-title">
                                <h4>Контакты</h4>
                            </div>
                            <div class="footer-text">
                                <ul>
                                    @if (!empty($settings->config['address']))
                                        <li>
                                            <i class="mdi mdi-map-marker"></i>
                                            <p>Адрес</p>
                                            <p>{{ $settings->config['address'] }}</p>
                                        </li>
                                    @endif
                                    @if (!empty($settings->config['phone']))
                                        <li>
                                            <i class="mdi mdi-phone"></i>
                                            <p>Телефон</p>
                                            <p>{{ $settings->config['phone'] }}</p>
                                        </li>
                                    @endif
                                    @if (!empty($settings->config['email']))
                                        <li>
                                            <i class="mdi mdi-email"></i>
                                            <p>Почта</p>
                                            <p>{{ $settings->config['email'] }}</p>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    @if ($pages->isNotEmpty())
                        <div class="col-xs-12 col-sm-6 col-md-6 r-margin-top wide-mobile">
                            <div class="single-text">
                                <div class="footer-title">
                                    <h4>Информация</h4>
                                </div>
                                <div class="footer-menu">
                                    <ul>
                                        @foreach($pages as $page)
                                            <li><a href="{{ route('store.page', $page->url_key) }}"><i class="mdi mdi-menu-right"></i>{{ $page->description->title }}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="col-xs-12 col-sm-6 col-md-3 r-margin-top wide-mobile">
                        <div class="single-text">
                            <div class="footer-title">
                                <h4>instagram</h4>
                            </div>
                            <div class="clearfix instagram">
                                <ul>
                                    <li><a href="#"><img src="/img/footer/in1.png" alt="Instagram" /></a></li>
                                    <li><a href="#"><img src="/img/footer/in2.png" alt="Instagram" /></a></li>
                                    <li><a href="#"><img src="/img/footer/in3.png" alt="Instagram" /></a></li>
                                    <li><a href="#"><img src="/img/footer/in4.png" alt="Instagram" /></a></li>
                                    <li><a href="#"><img src="/img/footer/in5.png" alt="Instagram" /></a></li>
                                    <li><a href="#"><img src="/img/footer/in6.png" alt="Instagram" /></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer-top area end -->
    <!-- footer-bottom area start -->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <p>&copy; Sellshop 2016. All Rights Reserved.</p>
                </div>
                <div class="col-xs-12 col-sm-6 text-right">
                    <a href="#"><img src="/img/footer/payment.png" alt="" /></a>
                </div>
            </div>
        </div>
    </div>
    <!-- footer-bottom area end -->
</footer>