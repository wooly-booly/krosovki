<header class="header-one header-two header-page">
    <div class="header-top-two">
        <div class="container text-center">
            <div class="row">
                <div class="col-sm-12">
                    <div class="middel-top">
                        <div class="left floatleft">
                            <p><i class="mdi mdi-clock"></i> Mon-Fri : 09:00-19:00</p>
                        </div>
                    </div>
                    <div class="middel-top clearfix">
                        <ul class="clearfix right floatright">
                            <li>
                                <a href="#"><i class="mdi mdi-account"></i></a>
                                <ul>
                                    @foreach ($customer_menu->getItems() as $link)
                                        <li><a href="{{ $link['path'] }}">{{ $link['label'] }}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <li>
                                <a  href="#"><i class="mdi mdi-settings"></i></a>
                                <ul>
                                    @foreach ($cart_menu->getItems() as $link)
                                        <li><a href="{{ $link['path'] }}">{{ $link['label'] }}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                        <div class="right floatright">
                            <form action="#" method="get">
                                <button type="submit"><i class="mdi mdi-magnify"></i></button>
                                <input type="text" placeholder="Search within these results..." />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container text-center">
        <div class="row">
            <div class="col-sm-2">
                <div class="logo">
                    <a href="{{ route('store') }}"><img src="/img/logo.png" alt="Sellshop" /></a>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="header-middel">
                    <div class="mainmenu">
                        <nav>
                            <ul>
                                <li><a href="{{ route('store.product.list') }}">Главная</a></li>
                                {{--<li><a href="shop.html">Shop</a>
                                    <ul class="magamenu">
                                        <li class="banner"><a href="shop.html"><img src="/img/maga1.png" alt="" /></a></li>
                                        <li><h5>men’s wear</h5>
                                            <ul>
                                                <li><a href="#">Shirts & Top</a></li>
                                                <li><a href="#">Shoes</a></li>
                                                <li><a href="#">Dresses</a></li>
                                                <li><a href="#">Shemwear</a></li>
                                                <li><a href="#">Jeans</a></li>
                                                <li><a href="#">Sweaters</a></li>
                                                <li><a href="#">Jacket</a></li>
                                                <li><a href="#">Men Watch</a></li>
                                            </ul>
                                        </li>
                                        <li><h5>women’s wear</h5>
                                            <ul>
                                                <li><a href="#">Shirts & Tops</a></li>
                                                <li><a href="#">Shoes</a></li>
                                                <li><a href="#">Dresses</a></li>
                                                <li><a href="#">Shemwear</a></li>
                                                <li><a href="#">Jeans</a></li>
                                                <li><a href="#">Sweaters</a></li>
                                                <li><a href="#">Jacket</a></li>
                                                <li><a href="#">Women Watch</a></li>
                                            </ul>
                                        </li>
                                        <li class="banner"><a href="shop.html"><img src="/img/maga2.png" alt="" /></a></li>
                                    </ul>
                                </li>--}}
                                <li><a>Каталог</a>
                                    <ul class="dropdown">
                                        @foreach ($category_menu->getItems() as $link)
                                            <li><a href="{{ $link['path'] }}">{{ $link['label'] }}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                                <li><a href="{{ route('store.article.list') }}">Статьи</a></li>
                                {{-- <li><a href="{{ route('blog') }}">Блог</a></li> --}}
                            </ul>
                        </nav>
                    </div>
                    <!-- mobile menu start -->
                    <div class="mobile-menu-area">
                        <div class="mobile-menu">
                            <nav id="dropdown">
                                <ul>
                                    <li><a href="{{ route('store.product.list') }}">Главная</a></li>
                                    {{--<li><a href="shop.html">Shop</a>
                                        <ul>
                                            <li><h5>men’s wear</h5>
                                                <ul>
                                                    <li><a href="#">Shirts & Top</a></li>
                                                    <li><a href="#">Shoes</a></li>
                                                    <li><a href="#">Dresses</a></li>
                                                    <li><a href="#">Shemwear</a></li>
                                                    <li><a href="#">Jeans</a></li>
                                                    <li><a href="#">Sweaters</a></li>
                                                    <li><a href="#">Jacket</a></li>
                                                    <li><a href="#">Men Watch</a></li>
                                                </ul>
                                            </li>
                                            <li><h5>women’s wear</h5>
                                                <ul>
                                                    <li><a href="#">Shirts & Tops</a></li>
                                                    <li><a href="#">Shoes</a></li>
                                                    <li><a href="#">Dresses</a></li>
                                                    <li><a href="#">Shemwear</a></li>
                                                    <li><a href="#">Jeans</a></li>
                                                    <li><a href="#">Sweaters</a></li>
                                                    <li><a href="#">Jacket</a></li>
                                                    <li><a href="#">Women Watch</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    --}}
                                    <li><a>Каталог</a>
                                        <ul>
                                            @foreach ($category_menu->getItems() as $link)
                                                <li><a href="{{ $link['path'] }}">{{ $link['label'] }}</a></li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    {{-- <li><a href="{{ route('blog') }}">Блог</a></li> --}}
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                @include('store.partials.cart')
            </div>
        </div>
    </div>
</header>
<!-- header section end -->

<!-- pages-title-start -->
@section('page-header-title')
    <div class="pages-title section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="pages-title-text text-center">
                        <h2>@yield('page-title')</h2>
                        @yield('breadcrumbs')
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@yield('page-header-title')
<!-- pages-title-end -->