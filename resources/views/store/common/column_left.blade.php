<div class="col-xs-12 col-sm-4 col-md-3">
    <div class="sidebar left-sidebar">

        @include('store.module.category')
        @include('store.module.brand')
        @if (!isset($disable_banner))
            @include('store.module.banner.2words')
        @endif

    </div>
</div>