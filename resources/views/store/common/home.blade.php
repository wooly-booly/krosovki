@extends('store.base')

@section('page-header-title')
@stop

@section('content')

    @include('store.module.banner.carousel')
    @include('store.module.banner.collection')

    <section class="single-products products-two section-padding extra-padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="section-title text-center">
                        <h2>Новинки</h2>
                    </div>
                </div>
            </div>
            <div class="wrapper">
                <ul class="load-list load-list-one">
                    @foreach($newProducts->chunk(4) as $productsRow)
                    <li style="display: list-item;">
                        <div class="row text-center">
                            @foreach($productsRow as $newProduct)
                            <!-- single product start -->
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="single-product">
                                    <div class="product-img">
                                        {{-- <div class="pro-type">
                                            <span>new</span>
                                        </div> --}}
                                        <a href="{{ route('store.product', ['product' => $newProduct->id, 'product_url_key' => $newProduct->url_key]) }}"><img src="{{ $newProduct->present()->thumb }}" alt="{{ $newProduct->description->title }}"></a>
                                        <div class="actions-btn">
                                            <a href="{{ route('store.cart.add', $newProduct->id) }}" onclick="cart.add(event, this);"><i class="mdi mdi-cart"></i></a>
                                            <a href="{{ route('store.product.quickview', $newProduct->id) }}" data-toggle="modal" data-target="#quick-view"><i class="mdi mdi-eye"></i></a>
                                            <a href="{{ route('store.wishlist.add', $newProduct->id) }}"><i class="mdi mdi-heart"></i></a>
                                        </div>
                                    </div>
                                    <div class="product-dsc">
                                        <p><a href="{{ route('store.product', ['product' => $newProduct->id, 'product_url_key' => $newProduct->url_key]) }}">{{ $newProduct->brand->title ?? '' }} {{ $newProduct->description->title }}</a></p>
                                        <span>{{ $newProduct->price }}</span>
                                    </div>
                                </div>
                            </div>
                            <!-- single product end -->
                            @endforeach
                        </div>
                    </li>
                    @endforeach
                </ul>
                <button id="load-more-one">Показать все</button>
            </div>
        </div>
    </section>
    
    @if ($articles->count())
    <section class="latest-blog section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="section-title text-center">
                        <h2>Статьи</h2>
                    </div>
                </div>
            </div>
            <div class="wrapper">
                <ul class="load-list load-list-blog">
                    @foreach ($articles->chunk(3) as $chunk)
                    <li style="display: list-item;">
                        <div class="row">
                            @foreach ($chunk as $article)
                            <div class="col-sm-4">
                                <div class="l-blog-text">
                                    <div class="banner"><a href="{{ route('store.article', ['article' => $article->id, 'url_key' => $article->url_key]) }}"><img src="{{ $article->present()->thumb  }}" alt="{{ $article->title }}" /></a></div>
                                    <div class="s-blog-text">
                                        <h4><a href="{{ route('store.article', ['article' => $article->id, 'url_key' => $article->url_key]) }}">{{ $article->description->title }}</a></h4>
                                        <span>Просмотров: {{ $article->viewed }}</span>
                                        <p>{{ $article->description->excerpt }}</p>
                                    </div>
                                    <div class="date-read clearfix">
                                        <a href="{{ route('store.article', ['article' => $article->id, 'url_key' => $article->url_key]) }}"><i class="mdi mdi-clock"></i>{{ $article->present()->published_date }}</a>
                                        <a href="{{ route('store.article', ['article' => $article->id, 'url_key' => $article->url_key]) }}">Подробнее</a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </li>
                    @endforeach
                </ul>
                <button id="load-more-blog">Показать все</button>
            </div>
        </div>
    </section>
    @endif

    @include('store.partials.quickview_modal')

@stop
