@extends('store.base')

@section('breadcrumbs', Breadcrumbs::render($breadcrumbs->route, $breadcrumbs->option ?? ''))
@section('page-title', $page_title)

@section('meta')
    <meta name="description" content="{{ isset($category) ? $category->description->meta_description : '' }}">
    <meta name="keywords" content="{{ isset($category) ? $category->description->meta_keyword : '' }}">
@stop

@section('content')
<section class="pages products-page section-padding-bottom section-padding-top">
    <div class="container">
        <div class="row">
            @include('store.common.column_left')

            <div class="col-xs-12 col-sm-8 col-md-9">
                <div class="right-products">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="section-title clearfix">                                   
                                <ul>
                                    <li>
                                        <ul class="nav-view">
                                            <li class="active"><a data-toggle="tab" href="#grid"> <i class="mdi mdi-view-module"></i> </a></li>
                                            {{-- <li><a data-toggle="tab" href="#list"> <i class="mdi mdi-view-list"></i> </a></li> --}}
                                        </ul>
                                    </li>
                                    <li class="sort-by floatright">
                                        @if ($products->total() > 1)
                                            Показано {{ $products->firstItem() }} - {{ $products->lastItem() }} из {{ $products->total() }} товаров
                                        @endif
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="tab-content grid-content">
                            <div class="tab-pane fade in active text-center" id="grid">
                                @foreach ($products as $product)
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <div class="single-product">
                                        <div class="product-img">
                                            {{-- <div class="pro-type sell">
                                                <span>sell</span>
                                            </div> --}}
                                            <a href="{{ route('store.product', ['product' => $product->id, 'product_url_key' => $product->url_key]) . (isset($category) ? '?cat=' . $category->id : '') }}"><img src="{{ $product->present()->thumb }}" alt="{{ $product->description->title }}" /></a>
                                            <div class="actions-btn">

                                                <a href="{{ route('store.cart.add', $product->id) }}" onclick="cart.add(event, this);"><i class="mdi mdi-cart"></i></a>
                                                <a href="{{ route('store.product.quickview', $product->id) }}" data-toggle="modal" data-target="#quick-view" class="qv-product"><i class="mdi mdi-eye"></i></a>
                                                <a href="{{ route('store.wishlist.add', $product->id) }}"><i class="mdi mdi-heart"></i></a>
                                            </div>
                                        </div>
                                        <div class="product-dsc">
                                            <p><a href="{{ route('store.product', ['product' => $product->id, 'product_url_key' => $product->url_key]) . (isset($category) ? '?cat=' . $category->id : '') }}">{{ $product->brand->title ?? '' }} {{ $product->description->title }}</a></p>
                                            {{-- <div class="ratting">
                                                <i class="mdi mdi-star"></i>
                                                <i class="mdi mdi-star"></i>
                                                <i class="mdi mdi-star"></i>
                                                <i class="mdi mdi-star-half"></i>
                                                <i class="mdi mdi-star-outline"></i>
                                            </div> --}}
                                            <span>{{ $product->price }}</span>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                <!-- single product end -->
                            </div>
                            {{--    <div class="tab-pane fade in" id="list">
                                <div class="col-xs-12">
                                    <div class="single-list-view">
                                        <div class="row">
                                            <div class="col-xs-12 col-md-4">
                                                <div class="list-img">
                                                    <div class="product-img">
                                                        <div class="pro-type sell">
                                                            <span>sell</span>
                                                        </div>
                                                        <a href="#"><img src="img/products/8.jpg" alt="Product Title" /></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-8">
                                                <div class="list-text">
                                                    <h3>men’s Black t-shirt</h3>
                                                    <span>Summer men’s fashion</span>
                                                    <div class="ratting floatright">
                                                        <p>( 27 Rating )</p>
                                                        <i class="mdi mdi-star"></i>
                                                        <i class="mdi mdi-star"></i>
                                                        <i class="mdi mdi-star"></i>
                                                        <i class="mdi mdi-star-half"></i>
                                                        <i class="mdi mdi-star-outline"></i>
                                                    </div>
                                                    <h5><del>$79.30</del> $69.30</h5>
                                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have be suffered alteration in some form, by injected humour, or randomised words which donot look even slightly believable. If you are going to use a passage of Lorem Ipsum, you neede be sure there isn't anything embarrassing.</p>
                                                    <div class="list-btn">
                                                        <a href="#">add to cart</a>
                                                        <a href="#">wishlist</a>
                                                        <a href="#" data-toggle="modal" data-target="#quick-view">zoom</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- single product end -->
                                </div>
                            </div> --}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            {{ $products->links() }}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</section>

@include('store.partials.quickview_modal')

@stop