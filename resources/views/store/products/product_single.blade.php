@extends('store.base')

@section('breadcrumbs', Breadcrumbs::render('store.product', $product, isset($category) ? $category : false))
@section('page-title', $page_title)

@section('meta')
    <meta name="description" content="{{ $product->description->meta_description }}">
    <meta name="keywords" content="{{ $product->description->meta_keyword }}">
@stop

@section('content')
    <section class="product-details pages section-padding-top section-padding-bottom">
        <div class="container">
            <div class="row">
                <div class="single-list-view">
                    @include('store.partials.product_description', $product)
                </div>
            </div>
        </div>
    </section>
@stop      
