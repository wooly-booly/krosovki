<div class="modal-footer" data-dismiss="modal">
    <span>x</span>
</div>
<div class="row">
    @include('store.partials.product_description', $product)
    <script type="text/javascript">
        $('#simpleLensSection .simpleLens-big-image').simpleLens();
    </script>
</div>