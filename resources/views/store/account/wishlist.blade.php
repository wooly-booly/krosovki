@extends('store.base')

@push('js')
    <script type="text/javascript">
        $(document).ready(function () {

            // Delete Item
            $('.delete-item').on('click', function() {
                var $this = $(this);
                var url = '{{ route("store.wishlist.remove", ["product" => '#id_delete']) }}';
                url = url.replace("#id_delete", $this.data('id'));

                $this.parents('tr').remove();

                if ( $('#wishlist-table tr').length < 2 ) {
                    $('#wishlist-table').hide();
                    $('#wishlist-empty').removeClass('hide');
                }

                $.ajax({
                    type: 'get',
                    url: url
                });
            });
        });
    </script>
@endpush

@section('breadcrumbs', Breadcrumbs::render('store.wishlist'))
@section('page-title', $page_title)

@section('content')
    <!-- wishlist content section start -->
    <section class="pages wishlist-page section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive padding60">
                        <h5 class="text-center {{ count($products) ? 'hide' : '' }}" id="wishlist-empty">В Избранном пусто</h5>
                        <table id="wishlist-table" class="text-center {{ count($products) ? '' : 'hide' }}">
                            <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>Цена</th>
                                    {{-- <th>Stock Status </th> --}}
                                    <th>Добавить в корзину</th>
                                    <th>Удалить</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                <tr>
                                    <td class="td-img text-left">
                                        @if ($product->options->image)
                                            <a href="{{ route('store.product', $product->id) }}"><img src="{{ $product->options->image }}" alt="{{ $product->name }}" /></a>        
                                        @endif
                                        <div class="items-dsc">
                                            <h5><a href="{{ route('store.product', $product->id) }}">{{ $product->name }}</a></h5>
                                            @foreach($product->options as $option => $value)
                                                @if ($option == 'image')
                                                    @continue
                                                @elseif ($option == 'brand')
                                                    <p class="itemcolor"><span>{{ $value }}</span></p>
                                                @else
                                                    <p class="itemcolor">{{ $option }} : <span>{{ $value }}</span></p>
                                                @endif
                                            @endforeach
                                        </div>
                                    </td>
                                    <td>{{ $product->price }}</td>
                                    {{-- <td>In Stock</td> --}}
                                    <td>
                                        <div class="submit-text">
                                            <a href="{{ route('store.cart.add', $product->id) }}">В корзину</a>
                                        </div>
                                    </td>
                                    <td><i class="mdi mdi-close delete-item" title="Удалить" data-id="{{ $product->rowId }}"></i></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- wishlist content section end -->
@stop
