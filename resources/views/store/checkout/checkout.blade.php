@extends('store.base')

@section('breadcrumbs', Breadcrumbs::render('store.checkout'))
@section('page-title', $page_title)

@section('content')
<section class="pages checkout section-padding">
    <div class="container">
        <form action="{{ route('store.checkout.confirm') }}" method="post">
            {{ csrf_field() }}
            <div class="row">
                <!-- client details section -->
                <div class="col-sm-6">
                    <div class="main-input single-cart-form padding60">
                        <div class="log-title">
                            <h3><strong>Информация о клиенте</strong></h3>
                        </div>
                        <div class="custom-input">                        
                            @if ($errors->has('firstname'))
                                <label class="error" for="firstname">{{ $errors->first('firstname') }}</label>
                            @endif
                            <input name="firstname" placeholder="* Имя" type="text" value="{{ old('firstname') }}">
                            @if ($errors->has('lastname'))
                                <label class="error" for="lastname">{{ $errors->first('lastname') }}</label>
                            @endif
                            <input name="lastname" placeholder="* Фамилия" type="text" value="{{ old('lastname') }}">
                            @if ($errors->has('email'))
                                <label class="error" for="email">{{ $errors->first('email') }}</label>
                            @endif
                            <input name="email" placeholder="* Email" type="text" value="{{ old('email') }}">
                            @if ($errors->has('phone'))
                                <label class="error" for="phone">{{ $errors->first('phone') }}</label>
                            @endif
                            <input name="phone" placeholder="* Телефон" type="text" value="{{ old('phone') }}">
                        </div>
                    </div>
                </div>
                <!-- client details end -->

                <!-- delivery details section -->
                <div class="col-sm-6">
                    <div class="main-input single-cart-form padding60">
                        <div class="log-title">
                            <h3><strong>Способ доставки</strong></h3>
                        </div>
                        <div class="custom-input">
                            @if ($errors->has('city'))
                                <label class="error" for="city">{{ $errors->first('city') }}</label>
                            @endif
                            <input name="city" placeholder="* Город" type="text" value="{{ old('city') }}">
                            @if ($errors->has('address'))
                                <label class="error" for="address">{{ $errors->first('address') }}</label>
                            @endif
                            <input name="address" placeholder="* Номер отделения 'Новой Почты'" type="text" value="{{ old('address') }}">
                            @if ($errors->has('comment'))
                                <label class="error" for="comment">{{ $errors->first('comment') }}</label>
                            @endif
                            <div class="custom-mess">
                                <textarea rows="2" placeholder="Комментарий к заказу" name="comment">{{ old('comment') }}</textarea>
                            </div>                       
                        </div>
                    </div>
                </div>
            </div>
            <!-- delivery details end -->
            
            <div class="row margin-top">
                <!-- order details section -->
                <div class="col-xs-12 col-sm-6">
                    <div class="padding60">
                        <div class="log-title">
                            <h3><strong>Ваш Заказ</strong></h3>
                        </div>
                        <div class="cart-form-text pay-details table-responsive">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Товар</th>
                                        <td>Цена</td>
                                        <td>Всего</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($cart->content() as $product)
                                        <tr>
                                            <th>{{ $product->name }}</th>
                                            <td>{{ $product->qty . ' x ' . $product->price }}</td>
                                            <td>{{ $product->price * $product->qty }} грн.</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Всего</th>
                                        <td></td>
                                        <td>{{ $cart->total() }} грн.</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- order details end -->

                <!-- payment details section -->
                <div class="col-xs-12 col-sm-6">
                    <div class="padding60">
                        <div class="log-title">
                            <h3><strong>Оплата</strong></h3>
                        </div>
                        <div class="categories">
                            <ul id="accordion" class="panel-group clearfix">
                                <li class="panel">
                                    <div data-toggle="collapse" data-parent="#accordion" data-target="#collapse1" aria-expanded="true" class="">
                                        <div class="medium-a">
                                            Придумай, бля
                                        </div>
                                    </div>
                                    <div class="panel-collapse collapse in" id="collapse1" aria-expanded="true" style="">
                                        <div class="normal-a">
                                            <p>Lorem Ipsum is simply in dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="panel">
                                    <div data-toggle="collapse" data-parent="#accordion" data-target="#collapse2" class="collapsed" aria-expanded="false">
                                        <div class="medium-a">
                                            Что здесь
                                        </div>
                                    </div>
                                    <div class="paypal-dsc panel-collapse collapse" id="collapse2" aria-expanded="false" style="height: 0px;">
                                        <div class="normal-a">
                                            <p>Lorem Ipsum is simply in dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="panel">
                                    <div data-toggle="collapse" data-parent="#accordion" data-target="#collapse3" class="collapsed" aria-expanded="false">
                                        <div class="medium-a">
                                            Писать?
                                        </div>
                                    </div>
                                    <div class="paypal-dsc panel-collapse collapse" id="collapse3" aria-expanded="false">
                                        <div class="normal-a">
                                            <p>Lorem Ipsum is simply in dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <div class="submit-text">
                                <button type="submit">Подтвердить заказ</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- payment details end -->
            </div>
        </form>
    </div>
</section>
@stop