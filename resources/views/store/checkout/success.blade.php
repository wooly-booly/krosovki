@extends('store.base')

@section('breadcrumbs', Breadcrumbs::render('store.checkout.success'))
@section('page-title', $page_title)

@section('content')
<section class="pages checkout section-padding">
    <div class="container">
        <div class="col-sm-12 col-md-8 col-md-offset-2">
            <div class="row">
                <div class="main-padding padding60 clearfix">
                    <div class="col-sm-12 col-md-4">
                        <div class="about-img">
                            <img src="/img/checkout/consultant.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-8">
                        <div class="about-text">
                            <div class="about-author">
                                <h4>Ваш заказ принят!</h4>
                            </div>
                            <p>Ожидайте, в ближайшее время с Вами свяжется наш консультант.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop