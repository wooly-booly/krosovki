@extends('store.base')

@push('js')
    <script type="text/javascript">
        $(document).ready(function () {
            // Delete Item
            $('.delete-item').on('click', function() {
                var $this = $(this);
                var url = '{{ route("store.cart.remove", ["product" => '#id_delete']) }}';
                url = url.replace("#id_delete", $this.parents('tr').data('id'));

                $this.parents('tr').remove();

                if ( $('#cart-table tr').length < 2 ) {
                    $('#cart-table').hide();
                    $('#cart-empty').removeClass('hide');
                }

                $.ajax({
                    type: 'post',
                    url: url
                }).done(function() {
                    location = "{{ route("store.cart") }}";
                });
            });

            // Update Item
            $('.update-item').on('click', function() {
                var $this = $(this);
                var quantity = $this.prev().prev().val();
                var url = '{{ route("store.cart.update", ["product" => '#id_update']) }}';
                url = url.replace("#id_update", $this.parents('tr').data('id'));

                $.ajax({
                    type: 'get',
                    url: url,
                    data: 'quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
                }).done(function() {
                    location = "{{ route("store.cart") }}";
                });
            });
        });    
    </script>
@endpush

@section('breadcrumbs', Breadcrumbs::render('store.cart'))
@section('page-title', $page_title)

@section('content')
    <!-- cart content section start -->
    <section class="pages cart-page section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive padding60">
                        <h5 class="text-center {{ count($products) ? 'hide' : '' }}" id="cart-empty">В Корзине пусто</h5>
                        <table id="cart-table" class="text-center {{ count($products) ? '' : 'hide' }}">
                            <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>Цена</th>
                                    <th>Количество</th>
                                    <th>Всего</th>
                                    <th>Удалить</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- cart items section start --}}
                                @foreach($products as $product)
                                <tr data-id="{{ $product->rowId }}">                                    
                                    <td class="td-img text-left">
                                        @if ($product->options->image)
                                            <a><img src="{{ $product->options->image }}" alt="{{ $product->name }}" /></a>
                                        @endif
                                        <div class="items-dsc">
                                            <h5><a href="{{ route('store.product', $product->id) }}">{{ $product->name }}</a></h5>
                                            @foreach($product->options as $option => $value)
                                                @if ($option == 'image')
                                                    @continue
                                                @elseif ($option == 'brand')
                                                    <p class="itemcolor"><span>{{ $value }}</span></p>
                                                @else
                                                    <p class="itemcolor">{{ $option }} : <span>{{ $value }}</span></p>
                                                @endif
                                            @endforeach
                                        </div>
                                    </td>
                                    <td>{{ $product->price }}</td>
                                    <td>
                                        <div class="plus-minus">
                                            <a class="dec qtybutton">-</a>
                                            <input type="text" value="{{ $product->qty }}" class="plus-minus-box">
                                            <a class="inc qtybutton">+</a>&nbsp;
                                            <i class="mdi mdi-autorenew update-item" title="Обновить"></i>
                                        </div>
                                    </td>
                                    <td>
                                        <strong>{{ $product->price * $product->qty }}</strong>
                                    </td>
                                    <td><i class="mdi mdi-close delete-item" title="Удалить"></i></td>
                                </tr>
                                @endforeach
                                {{-- cart items section end --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-sm-6">
                    <div class="single-cart-form padding60">
                        <div class="log-title">
                            <h3><strong>coupon discount</strong></h3>
                        </div>
                        <div class="cart-form-text custom-input">
                            <p>Enter your coupon code if you have one!</p>
                            <form action="mail.php" method="post">
                                <input type="text" name="subject" placeholder="Enter your code here..." />
                                <div class="submit-text coupon">
                                    <button type="submit">apply coupon </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="single-cart-form padding60">
                        <div class="log-title">
                            <h3><strong>payment details</strong></h3>
                        </div>
                        <div class="cart-form-text pay-details table-responsive">
                            <table>
                                <tbody>
                                    <tr>
                                        <th>Cart Subtotal</th>
                                        <td>$155.00</td>
                                    </tr>
                                    <tr>
                                        <th>Shipping and Handing</th>
                                        <td>$15.00</td>
                                    </tr>
                                    <tr>
                                        <th>Vat</th>
                                        <td>$00.00</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="tfoot-padd">Order total</th>
                                        <td class="tfoot-padd">$170.00</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row margin-top">
                <div class="col-xs-12">
                    <div class="padding60">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="single-cart-form">
                                    <div class="log-title">
                                        <h3><strong>calculate shipping</strong></h3>
                                    </div>
                                    <div class="cart-form-text custom-input">
                                        <p>Enter your coupon code if you have one!</p>
                                        <form action="mail.php" method="post">
                                            <input type="text" name="country" placeholder="Country" />
                                            <div class="submit-text">
                                                <button type="submit" >get a quote</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>  
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="single-cart-form">
                                    <div class="cart-form-text post-state custom-input">
                                        <form action="mail.php" method="post">
                                            <input type="text" name="state" placeholder="Region / State" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="single-cart-form">
                                    <div class="cart-form-text post-state custom-input">
                                        <form action="mail.php" method="post">
                                            <input type="text" name="subject" placeholder="Post Code" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- cart content section end -->
@stop
