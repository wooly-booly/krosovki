@if ($breadcrumbs)
    <ul class="text-left">
        @foreach ($breadcrumbs as $breadcrumb)
            @if ($breadcrumb->url && !$breadcrumb->last)
                <li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a> <span>/</span> </li>
            @else
                <li>{{ $breadcrumb->title }}</li>
            @endif
        @endforeach
    </ul>
    <br>
@endif