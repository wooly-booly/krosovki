@push('js')
    <script type="text/javascript">
        $(document).ready(function () {
            /*** Quick View ***/
            // to reupload modal with new ajax data
            $('#quick-view').on('hidden.bs.modal', function() {
                $(this).removeData('bs.modal');
            });
        });
    </script>
@endpush
<!-- quick view start -->
<div class="product-details quick-view modal animated zoomInUp in" id="quick-view">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="d-table">
                    <div class="d-tablecell">
                        <div class="modal-dialog">
                            <div class="main-view modal-content">
                                <div class="modal-footer" data-dismiss="modal">
                                    <span>x</span>
                                </div>
                                <div class="row">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <!-- quick view end -->