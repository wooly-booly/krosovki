@if ($related_products->count())
<!-- related-products section start -->
<section class="single-products section-padding-bottom">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-title text-center">
                    <h2>{{ $title or 'Похожие товары' }}</h2>
                </div>
            </div>
        </div>
        <div class="row text-center">
            @foreach ($related_products as $product)
            <!-- single product start -->
            <div class="col-xs-12 col-sm-6 col-md-3 r-margin-top">
                <div class="single-product">
                    <div class="product-img">
                        {{-- <div class="pro-type sell">
                            <span>sell</span>
                        </div> --}}
                        <a href="{{ route('store.product', ['product' => $product->id, 'product_url_key' => $product->url_key]) }}"><img src="{{ $product->present()->thumb }}" alt="{{ $product->description->title }}" /></a>
                        <div class="actions-btn">

                            <a href="{{ route('store.cart.add', $product->id) }}" onclick="cart.add(event, this);"><i class="mdi mdi-cart"></i></a>
                            <a href="{{ route('store.product.quickview', $product->id) }}" data-toggle="modal" data-target="#quick-view" class="qv-product"><i class="mdi mdi-eye"></i></a>
                            <a href="{{ route('store.wishlist.add', $product->id) }}"><i class="mdi mdi-heart"></i></a>
                        </div>
                    </div>
                    <div class="product-dsc">
                        <p><a href="{{ route('store.product', ['product' => $product->id, 'product_url_key' => $product->url_key]) }}">{{ $product->brand->title ?? '' }} {{ $product->description->title }}</a></p>
                        <span>{{ $product->price }}</span>
                    </div>
                </div>
            </div>
            <!-- single product end -->
            @endforeach
        </div>
    </div>
</section>
@include('store.partials.quickview_modal')
<!-- related-products section end -->
@endif