@if (!empty($item['children']))
    <div class="normal-a dropdown-submenu">
        <a href="{{ route('store.category', ['category_url_key' => $item['url_key'], 'category' => $item['id']]) }}">{{ $item['description']['title'] }}</a>
        <div class="dropdown-menu normal-a">
            @foreach($item['children'] as $item)
                @include('store.partials.sidebar_category_menu', $item)
            @endforeach
        </div>
    </div>
@else
    <a href="{{ route('store.category', ['category_url_key' => $item['url_key'], 'category' => $item['id']]) }}">{{ $item['description']['title'] }}</a>
@endif
