<div class="col-xs-12 col-sm-5 col-md-4">
    <div class="quick-image">
        <div class="single-quick-image text-center" id="simpleLensSection">
            <div class="list-img">
                <div class="product-img tab-content">
                    <div class="simpleLens-container active tab-pane fade in" id="sin">
                        {{-- <div class="pro-type sell">
                            <span>sell</span>
                        </div> --}}
                        <a class="simpleLens-image" data-lens-image="{{ $product->image }}"><img src="{{ $product->image }}" alt="{{ $product->description->title }}" class="simpleLens-big-image"></a>
                    </div>
                    @foreach ($product->images as $img)
                        <div class="simpleLens-container tab-pane fade in" id="sin-{{ $img->id }}">
                            <a class="simpleLens-image" data-lens-image="{{ $img->image }}"><img src="{{ $img->image }}" alt="{{ $product->description->title }}" class="simpleLens-big-image"></a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="quick-thumb">
            <ul class="product-slider">
                <li class="active"><a data-toggle="tab" href="#sin"> <img src="{{ $product->present()->thumb }}" alt="quick view"> </a></li>
                @foreach ($product->present()->thumbs as $img)
                    <li><a data-toggle="tab" href="#sin-{{ $img->id }}"> <img src="{{ $img->image }}" alt="{{ $product->description->title }}"> </a></li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-7 col-md-8">
    <div class="quick-right">
        <div class="list-text">
            <h3>{{ $product->description->title }}</h3>
            @if ($product->brand)
                <span>{{ $product->brand->title }}</span>
            @endif
            {{-- <div class="ratting floatright">
                <p>( 27 Rating )</p>
                <i class="mdi mdi-star"></i>
                <i class="mdi mdi-star"></i>
                <i class="mdi mdi-star"></i>
                <i class="mdi mdi-star-half"></i>
                <i class="mdi mdi-star-outline"></i>
            </div> --}}
            <h5>{{-- <del>$79.30 </del> --}}{{ $product->price }}</h5>
            <p>{!! $product->description->description !!}</p>
            <div class="all-choose">
                <div class="s-shoose">
                    <h5>Color</h5>
                    <div class="color-select clearfix">
                        <span></span>
                        <span class="outline"></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="s-shoose">
                    <h5>size</h5>
                    <div class="size-drop">
                        <div class="btn-group">
                            <button type="button" class="btn">XL</button>
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class=""><i class="mdi mdi-chevron-down"></i></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Xl</a></li>
                                <li><a href="#">SL</a></li>
                                <li><a href="#">S</a></li>
                                <li><a href="#">L</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="s-shoose">
                    <h5>qty</h5>
                    <form action="#" method="POST">
                        <div class="plus-minus">
                            <a class="dec qtybutton">-</a>
                            <input value="1" name="qtybutton" class="plus-minus-box" type="text">
                            <a class="inc qtybutton">+</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="list-btn">
                <a href="{{ route('store.cart.add', $product->id) }}" onclick="cart.add(event, this);">В Корзину</a>
                <a href="{{ route('store.wishlist.add', $product->id) }}">В Избранное</a>
                {{-- <a data-toggle="modal" data-target="#quick-view">zoom</a> --}}
            </div>
            <div class="share-tag clearfix">
                <ul class="blog-share floatleft">
                    <li><h5>Поделиться </h5></li>
                    <li><a href="#"><i class="mdi mdi-vk"></i></a></li>
                    <li><a href="#"><i class="mdi mdi-facebook"></i></a></li>
                    {{-- <li><a href="#"><i class="mdi mdi-twitter"></i></a></li>
                    <li><a href="#"><i class="mdi mdi-linkedin"></i></a></li>
                    <li><a href="#"><i class="mdi mdi-vimeo"></i></a></li>
                    <li><a href="#"><i class="mdi mdi-dribbble"></i></a></li>
                    <li><a href="#"><i class="mdi mdi-instagram"></i></a></li> --}}
                </ul>
            </div>
        </div>
    </div>
</div>