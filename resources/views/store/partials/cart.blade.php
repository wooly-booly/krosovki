@inject('cart', 'Cart')

<div class="cart-itmes" id="cart">
    <a class="cart-itme-a" href="{{ route('store.cart') }}">
        <i class="mdi mdi-cart"></i>
        <span id="cart-info">Всего ({{ $cart::instance('shopping')->count() }}): <strong>{{ $cart::instance('shopping')->total() }} грн</strong></span>
    </a>
    <div class="cartdrop">
        <div style="overflow: auto; max-height: 28vh;">
            @foreach($cart::instance('shopping')->content() as $product)
            <div class="sin-itme clearfix">
                <i class="mdi mdi-close" data-id="{{ $product->rowId }}" onclick="cart.delete(this);"></i>
                <a class="cart-img" href="{{ route('store.product', $product->id) }}"><img src="{{ $product->options->image }}" alt="{{ $product->name }}"></a>
                <div class="menu-cart-text">
                    <a><h5>{{ $product->name . " (" . $product->qty . ")" }}</h5></a>
                    @foreach($product->options as $option => $value)
                        @if ($option == 'image')
                            @continue
                        @elseif ($option == 'brand')
                            <span>{{ $value }}</span>
                        @else
                            <span>{{ $option }} : {{ $value }}</span>
                        @endif
                    @endforeach
                    <strong>{{ $product->price }}</strong>
                </div>
            </div>
            @endforeach
        </div>
        <div class="total">
            <span>Всего <strong id="cart-total">= {{ $cart::instance('shopping')->total() }}</strong></span>
        </div>
        <a class="goto" href="{{ route('store.cart') }}">Перейти в корзину</a>
        <a class="out-menu" href="{{ route('store.checkout') }}">Оформить</a>
    </div>
</div>