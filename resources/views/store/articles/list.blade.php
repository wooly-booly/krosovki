@extends('store.base')

@section('breadcrumbs', Breadcrumbs::render('store.article.list'))
@section('page-title', $page_title)

@section('meta')
    <meta name="description" content="">
    <meta name="keywords" content="">
@stop

@section('content')

<!-- blog section start -->
<section class="latest-blog section-padding">
    <div class="container">
        <ul class="blog-row clearfix">
            @foreach ($articles->chunk(3) as $chunk)
            <li>
                <div class="row">
                    @foreach ($chunk as $article)
                    <div class="col-sm-4">
                        <div class="l-blog-text">
                            <div class="banner"><a href="{{ route('store.article', ['article' => $article->id, 'url_key' => $article->url_key]) }}"><img src="{{ $article->present()->thumb }}" alt="{{ $article->title }}" /></a></div>
                            <div class="s-blog-text">
                                <h4><a href="{{ route('store.article', ['article' => $article->id, 'url_key' => $article->url_key]) }}">{{ $article->description->title }}</a></h4>
                                <span>Просмотров: {{ $article->viewed }}</span>
                                <p>{{ $article->description->excerpt }}</p>
                            </div>
                            <div class="date-read clearfix">
                                <a href="{{ route('store.article', ['article' => $article->id, 'url_key' => $article->url_key]) }}"><i class="mdi mdi-clock"></i>{{ $article->present()->published_date }}</a>
                                <a href="{{ route('store.article', ['article' => $article->id, 'url_key' => $article->url_key]) }}">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </li>
            @endforeach
        </ul>
        <div class="row">
            <div class="col-sm-12">
                {{ $articles->links() }}
            </div>
        </div>
    </div>
</section>
<!-- blog section end -->

@stop