@extends('store.base')

@section('breadcrumbs', Breadcrumbs::render('store.article', $article))
@section('page-title', $page_title)

@section('meta')
    <meta name="description" content="{{ $article->description->meta_description }}">
    <meta name="keywords" content="{{ $article->description->meta_keyword }}">
@stop

@section('content')

<!-- blog-section-start -->
<section class="pages blog single-blog-area section-padding-top section-padding-bottom">
    <div class="container">
        <div class="row">
            @include('store.common.column_left', ['disable_banner' => true])
            <div class="col-xs-12 col-sm-8 col-md-9">
                <!-- single post start -->
                <div class="single-blog-page">
                    <div class="single-blog-img">
                        <img src="{{ $article->image }}" alt="$article->description->title" />
                    </div>
                    <div class="padding30">
                        <div class="blog-text">
                            <div class="post-title">
                                <h3>{{ $article->description->title }}</h3>
                                <ul class="clearfix">
                                    <li><i class="pe-7s-comment"></i><a>{{ $article->present()->published_date }}</a><span>|</span></li>
                                    <li><i class="pe-7s-back"></i><a>Просмотров: {{ $article->viewed }}</a></li>
                                </ul>
                            </div>
                            <p>
                                {!! $article->description->body !!}
                            </p>
                            <div class="share-tag clearfix">
                                <ul class="blog-share floatleft">
                                    <li><h5>share </h5></li>
                                    <li><a href="#"><i class="mdi mdi-facebook"></i></a></li>
                                    <li><a href="#"><i class="mdi mdi-twitter"></i></a></li>
                                    <li><a href="#"><i class="mdi mdi-linkedin"></i></a></li>
                                    <li><a href="#"><i class="mdi mdi-vimeo"></i></a></li>
                                    <li><a href="#"><i class="mdi mdi-dribbble"></i></a></li>
                                    <li><a href="#"><i class="mdi mdi-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- single post end -->
            </div>
        </div>
    </div>
</section>
<!-- blog section end -->

@if ($related_articles->count())
<!-- related post section start -->
<section class="related-post latest-blog section-padding-bottom">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-title text-center">
                    <h2>Cтатьи</h2>
                </div>
            </div>
        </div>
        <ul class="blog-row">
            @foreach ($related_articles->chunk(3) as $chunk)
            <li>
                <div class="row">
                    @foreach ($chunk as $related_article)
                    <div class="col-sm-4">
                        <div class="l-blog-text">
                            <div class="banner"><a href="{{ route('store.article', ['article' => $related_article->id, 'url_key' => $related_article->url_key]) }}"><img src="{{ $related_article->present()->thumb  }}" alt="{{ $related_article->title }}" /></a></div>
                            <div class="s-blog-text">
                                <h4><a href="{{ route('store.article', ['article' => $related_article->id, 'url_key' => $related_article->url_key]) }}">{{ $related_article->description->title }}</a></h4>
                                <span>Просмотров: {{ $related_article->viewed }}</span>
                                <p>{{ $related_article->description->excerpt }}</p>
                            </div>
                            <div class="date-read clearfix">
                                <a href="{{ route('store.article', ['article' => $related_article->id, 'url_key' => $related_article->url_key]) }}"><i class="mdi mdi-clock"></i>{{ $related_article->present()->published_date }}</a>
                                <a href="{{ route('store.article', ['article' => $related_article->id, 'url_key' => $related_article->url_key]) }}">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </li>
            @endforeach
        </ul>
    </div>
</section>
<!-- related post section end -->
@endif

@include('store.partials.related_products', ['related_products' => $related_products, 'title' => 'Товары'])

@stop