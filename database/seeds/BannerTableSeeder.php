<?php

use Illuminate\Database\Seeder;

class BannerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('banners')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('banners')->insert([
            ['id' => 1, 'title' => '2 words'],
            ['id' => 2, 'title' => 'Top 5'],
            ['id' => 3, 'title' => 'Carousel'],
            ['id' => 4, 'title' => 'Collection']
        ]);
    }
}
