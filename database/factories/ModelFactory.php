<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(WBstore\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(WBstore\Category::class, function (Faker\Generator $faker) {
    return [
        'url_key' => $faker->slug,
        'position' => $faker->randomDigitNotNull(),
        'status' => 1,
    ];
});

$factory->define(WBstore\CategoryDescription::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word,
        'description' => $faker->sentence(),
        'meta_description' => $faker->sentence(),
        'meta_keyword' => $faker->sentence(),
    ];
});

$factory->define(WBstore\Brand::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word,
        'url_key' => $faker->slug,
        // 'image' => $faker->,
        'position' => $faker->randomDigitNotNull(),
    ];
});

$factory->define(WBstore\Product::class, function (Faker\Generator $faker) {
    return [
        'url_key' => $faker->slug,
        'brand_id' => rand(1,5),
        'price' => $faker->randomFloat(2, 0, 1000),
        'position' => $faker->randomDigitNotNull(),
        'status' => 1,
    ];
});

$factory->define(WBstore\ProductDescription::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word,
        'description' => $faker->sentence(),
        'meta_description' => $faker->sentence(),
        'meta_keyword' => $faker->sentence(),
    ];
});