<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('categories', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('url_key')->unique()->nullable();
			$table->string('image')->nullable();
			$table->integer('parent_id')->unsigned()->nullable();
			$table->integer('real_depth', false, true);
            $table->integer('position', false, true)->default('0');
            $table->tinyInteger('status')->default('0');
			$table->timestamps();

            $table->foreign('parent_id')->references('id')->on('categories')->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
