<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('products', function (Blueprint $table) {
			$table->increments('id');
            $table->integer('brand_id')->unsigned()->index()->nullable();
            $table->string('url_key')->unique()->nullable();
            $table->string('image')->nullable();
            $table->decimal('price', 6, 2);
            $table->integer('position', false, true)->default('0');
            $table->tinyInteger('status')->default('0');
            $table->timestamps();
            
            $table->foreign('brand_id')->references('id')->on('brands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('products');
    }
}
